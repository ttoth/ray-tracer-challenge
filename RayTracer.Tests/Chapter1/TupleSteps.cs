﻿using RayTracer.Math;
using TechTalk.SpecFlow;
using Xunit;

namespace RayTracer.Tests.Chepter1;

public sealed class TupleSteps : CommonStepBase
{
    public TupleSteps(ScenarioContext context) : base(context) { }

    [Given(@"(\w+) ← ((?>vector|tuple|point|color)\(.*\))")]
    public void GivenATuple(string name, Tuple obj)
    {
        _context[name] = obj;
    }

    [Given(@"(.*) ← normalize\((.*)\)")]
    public void WhenNormailzed(string result, Tuple tuple)
    {
        _context[result] = tuple.Normalize();
    }

    [When(@"(.*) ← normalize\((\w+)\)")]
    public void WhenNormailzed(string result, string name)
    {
        _context[result] = Get<Tuple>(name).Normalize();
    }

    [Then(@"(.*) is a point")]
    public void ThenTupleIsAPoint(string name)
    {
        Assert.True(Get<Tuple>(name).IsPoint());
    }

    [Then(@"(.*) is not a point")]
    public void ThenTupleIsNotAPoint(string name)
    {
        Assert.False(Get<Tuple>(name).IsPoint());
    }

    [Then(@"(.*) is a vector")]
    public void ThenTupleIsAVector(string name)
    {
        Assert.True(Get<Tuple>(name).IsVector());
    }

    [Then(@"(.*) is not a vector")]
    public void ThenTupleIsNotAVector(string name)
    {
        Assert.False(Get<Tuple>(name).IsVector());
    }

    [Then(@"(\w+) = ((?>vector|tuple|point|color)\(.*\))")]
    public void ThenEquals(string name, Tuple expected)
    {
        Assert.Equal(expected, Get<Tuple>(name));
    }

    [Then(@"([a-z]\w*) \+ ([a-z]\w*) = (.*)")]
    public void ThenSumEquals(string a, string b, Tuple expected)
    {
        var result = Get<Tuple>(a) + Get<Tuple>(b);
        Assert.Equal(expected, result);
    }

    [Then(@"([a-z]\w*) \- ([a-z]\w*) = (.*)")]
    public void ThenSubtractEquals(string a, string b, Tuple expected)
    {
        var result = Get<Tuple>(a) - Get<Tuple>(b);
        Assert.Equal(expected, result);
    }

    [Then(@"-(.*) = (.*)")]
    public void ThenNegateEquals(string name, Tuple expected)
    {
        var result = -Get<Tuple>(name);
        Assert.Equal(expected, result);
    }

    [Then(@"([a-z]\w*) \* ([\d\.]*) = (.*)")]
    public void ThenMultipliedByScalarEquals(string name, double multiplier, Tuple expected)
    {
        var result = Get<Tuple>(name) * multiplier;
        Assert.Equal(expected, result);
    }

    [Then(@"([a-z]\w*) \/ ([\d\.]*) = (.*)")]
    public void ThenDividedByScalarEquals(string name, double multiplier, Tuple expected)
    {
        var result = Get<Tuple>(name) / multiplier;
        Assert.Equal(expected, result);
    }

    [Then(@"magnitude\((.*)\) = (.*)")]
    public void ThenMagnitudeEquals(string name, double expected)
    {
        var result = Get<Tuple>(name).Magnitude();
        Assert.Equal(expected, result, 5);
    }

    [Then(@"normalize\((.*)\) = (.*)")]
    public void ThenNormalizedVectorEquals(string name, Tuple expected)
    {
        var result = Get<Tuple>(name).Normalize();
        Assert.Equal(expected, result);
    }

    [Then(@"dot\((.*), (.*)\) = (.*)")]
    public void ThenDotEquals(string a, string b, double expected)
    {
        var result = Get<Tuple>(a).Dot(Get<Tuple>(b));
        Assert.Equal(expected, result);
    }

    [Then(@"cross\((.*), (.*)\) = (.*)")]
    public void ThenCrossEquals(string a, string b, Tuple expected)
    {
        var result = Get<Tuple>(a).Cross(Get<Tuple>(b));
        Assert.Equal(expected, result);
    }
}
