﻿using RayTracer.Lights;
using RayTracer.Materials;
using RayTracer.Math;
using RayTracer.Patterns;
using RayTracer.Shapes;
using TechTalk.SpecFlow;

namespace RayTracer.Tests.Chapter10;

public sealed class MaterialSteps : CommonStepBase
{
    public MaterialSteps(ScenarioContext context) : base(context) { }

    [Given(@"(\w+)\.pattern ← stripe_pattern\((\w+\(.*?\)), (.*)\)")]
    public void GivenPatternStripePattern(string name, Tuple a, Tuple b)
    {
        Get<Material>(name).Pattern = new StripePattern(a, b);
    }

    [When(@"(\w+) ← lighting\((\w+), (\w+), (.*), (\w+), (\w+), (\w+)\)")]
    public void WhenC_LightingMLightPointEyevNormalvFalse(string name, string material, string light, Tuple point, string eye, string normal, bool inShadow)
    {
        _context[name] = new Sphere { Material = Get<Material>(material) }.Lighting(Get<PointLight>(light), point, Get<Tuple>(eye), Get<Tuple>(normal), inShadow);
    }

}
