﻿using RayTracer.Math;
using RayTracer.Math.Matrices;
using RayTracer.Patterns;
using RayTracer.Shapes;
using TechTalk.SpecFlow;
using Xunit;

namespace RayTracer.Tests.Chapter10;

public sealed class PatternSteps : CommonStepBase
{
    public PatternSteps(ScenarioContext context) : base(context) { }

    [Given(@"(\w+) ← stripe_pattern\((\w+), (\w+)\)")]
    public void GivenStripePattern(string name, string a, string b)
    {
        _context[name] = new StripePattern(Get<Tuple>(a), Get<Tuple>(b));
    }

    [Given(@"(\w+) ← test_pattern\(\)")]
    public void GivenTestPattern(string name)
    {
        _context[name] = new TestPattern();
    }

    [Given(@"(\w+) ← gradient_pattern\((\w+), (\w+)\)")]
    public void GivenGradientPattern(string name, string a, string b)
    {
        _context[name] = new GradientPattern(Get<Tuple>(a), Get<Tuple>(b));
    }

    [Given(@"(\w+) ← ring_pattern\((\w+), (\w+)\)")]
    public void GivenRingPattern(string name, string a, string b)
    {
        _context[name] = new RingPattern(Get<Tuple>(a), Get<Tuple>(b));
    }

    [Given(@"(\w+) ← checkers_pattern\((\w+), (\w+)\)")]
    public void GivenCheckerPattern(string name, string a, string b)
    {
        _context[name] = new CheckersPattern(Get<Tuple>(a), Get<Tuple>(b));
    }

    [Given(@"(\w+) ← solid_pattern\((\w+)\)")]
    public void GivenSolidPattern(string name, string color)
    {
        _context[name] = new SolidPattern(Get<Tuple>(color));
    }

    [Given(@"(\w+) ← radial_gradient_pattern\((\w+), (\w+)\)")]
    public void GivenRadialGradientPattern(string name, string a, string b)
    {
        _context[name] = new RadialGradientPattern(Get<Tuple>(a), Get<Tuple>(b));
    }

    [Given(@"(\w+) ← blended_pattern\((\w+), (\w+)\)")]
    public void GivenBlendedPattern(string name, string a, string b)
    {
        _context[name] = new BlendedPattern(Get<Pattern>(a), Get<Pattern>(b));
    }

    [Given(@"set_pattern_transform\((\w+), (\w+\(.*\))\)")]
    [When(@"set_pattern_transform\((\w+), (\w+\(.*\))\)")]
    public void WhenSetTransform(string pattern, Matrix44 matrix)
    {
        Get<Pattern>(pattern).Transform = matrix;
    }

    [When(@"(\w+) ← stripe_at_object\((\w+), (\w+), (.*)\)")]
    [When(@"(\w+) ← pattern_at_shape\((\w+), (\w+), (.*)\)")]
    public void WhenStripeAtObject(string name, string pattern, string shape, Tuple point)
    {
        _context[name] = Get<IPattern>(pattern).PatternAtShape(Get<IShape>(shape), point);
    }

    [Then(@"stripe_at\((\w+), (.*)\) = (\w+)")]
    [Then(@"pattern_at\((\w+), (.*)\) = (\w+)")]
    public void TheStripeAt(string pattern, Tuple point, string expected)
    {
        Assert.Equal(Get<Tuple>(expected), Get<Pattern>(pattern).PatternAt(point));
    }

    [Then(@"pattern_at\((\w+), (.*)\) = (\w+\(.*\))")]
    public void ThePatternAt(string pattern, Tuple point, Tuple expected)
    {
        Assert.Equal(expected, Get<Pattern>(pattern).PatternAt(point));
    }
}
