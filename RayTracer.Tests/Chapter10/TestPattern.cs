﻿using RayTracer.Math;
using RayTracer.Patterns;

namespace RayTracer.Tests.Chapter10;

public class TestPattern : Pattern
{
    public override Tuple PatternAt(Tuple point) => Tuple.Color(point.X, point.Y, point.Z);
}
