﻿using RayTracer.Shapes;

namespace RayTracer.Tests.Chapter11;

public class GlassSphere : Sphere
{
    public GlassSphere()
    {
        Material = Material with { Transparency = 1, RefractiveIndex = 1.5 };
    }
}
