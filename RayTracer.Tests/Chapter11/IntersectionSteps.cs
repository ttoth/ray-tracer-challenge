﻿using RayTracer.Math;
using RayTracer.Shapes;
using RayTracer.Tests.Transforms;
using RayTracer.Tracing;
using System;
using TechTalk.SpecFlow;
using Xunit;

namespace RayTracer.Tests.Chapter11;

public sealed class IntersectionSteps : CommonStepBase
{
    public IntersectionSteps(ScenarioContext context) : base(context) { }

    [Given(@"(\w+) ← intersections\((.*\:.*)\)")]
    public void GivenIntersectionsByShape(string name, string intersections)
    {
        var parts = Array.ConvertAll(intersections.Split(", "), s =>
        {
            var p = s.Split(':');
            return new Intersection(FloatTransforms.ParseFloat(p[0]), Get<IShape>(p[1]));
        });

        _context[name] = new Intersections(parts);
    }

    [Given(@"(\w+) ← intersections\((\w+)\)")]
    [When(@"(\w+) ← intersections\((\w+)\)")]
    public void GivenIntersections(string name, string i1)
    {
        _context[name] = new Intersections(Get<Intersection>(i1));
    }

    [When(@"(\w+) ← prepare_computations\((\w+)\[(.*)\], (\w+), (\w+)\)")]
    public void WhenPrepareComputations(string name, string xs, int index, string ray, string xs2)
    {
        _context[name] = Get<Intersections>(xs)[index].PrepareComputations(Get<Ray>(ray), Get<Intersections>(xs2));
    }

    [When(@"(\w+) ← prepare_computations\((\w+), (\w+), (\w+)\)")]
    public void WhenPrepareComputations(string name, string i, string ray, string xs)
    {
        _context[name] = Get<Intersection>(i).PrepareComputations(Get<Ray>(ray), Get<Intersections>(xs));
    }

    [When(@"(\w+) ← schlick\((\w+)\)")]
    public void WhenSchlickComputed(string name, string comps)
    {
        _context[name] = Get<ComputationData>(comps).Schlick();
    }

    [Then(@"(\w+)\.under_point\.z > EPSILON/2")]
    public void ThenUnderPointZGreaterThanEpsilon2(string name)
    {
        Assert.True(Get<ComputationData>(name).UnderPoint.Z > Float.Epsilon / 2);
    }

    [Then(@"(\w+)\.point\.z < (\w+)\.under_point\.z")]
    public void ThenCompsPointZLessThanCompsUnderPointZ(string name, string name2)
    {
        Assert.True(Get<ComputationData>(name).Point.Z < Get<ComputationData>(name2).UnderPoint.Z);
    }
}
