﻿using TechTalk.SpecFlow;

namespace RayTracer.Tests.Chapter11;

public sealed class SphereSteps : CommonStepBase
{
    public SphereSteps(ScenarioContext context) : base(context) { }

    [Given(@"(\w+) ← glass_sphere\(\)")]
    public void GivenGlassSphere(string name)
    {
        _context[name] = new GlassSphere();
    }

    [Given(@"(\w+) ← glass_sphere\(\) with:")]
    public void GivenGlassSphereWith(string name, GlassSphere sphere)
    {
        _context[name] = sphere;
    }
}
