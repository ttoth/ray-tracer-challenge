﻿Feature: World

Scenario: The reflected color for a nonreflective material
  Given w ← default_world()
    And r ← ray(point(0, 0, 0), vector(0, 0, 1))
    And shape ← the second object in w
    And shape.material.ambient ← 1
    And i ← intersection(1, shape)
   When comps ← prepare_computations(i, r)
    And color ← reflected_color(w, comps)
   Then color = color(0, 0, 0)

Scenario: The reflected color for a reflective material
  Given w ← default_world()
    And shape ← plane() with:
      | material.reflective | transform             |
      | 0.5                 | translation(0, -1, 0) |
    And shape is added to w
    And r ← ray(point(0, 0, -3), vector(0, -√2/2, √2/2))
    And i ← intersection(√2, shape)
   When comps ← prepare_computations(i, r)
    And color ← reflected_color(w, comps)
   Then color = color(0.19032, 0.2379, 0.14274)

Scenario: shade_hit() with a reflective material
  Given w ← default_world()
    And shape ← plane() with:
      | material.reflective | transform             |
      | 0.5                 | translation(0, -1, 0) |
    And shape is added to w
    And r ← ray(point(0, 0, -3), vector(0, -√2/2, √2/2))
    And i ← intersection(√2, shape)
   When comps ← prepare_computations(i, r)
    And color ← shade_hit(w, comps)
   Then color = color(0.87677, 0.92436, 0.82918)

Scenario: color_at() with mutually reflective surfaces
  Given w ← world()
    And w.light ← point_light(point(0, 0, 0), color(1, 1, 1))
    And lower ← plane() with:
      | material.reflective | transform             |
      |  1                  | translation(0, -1, 0) |
    And lower is added to w
    And upper ← plane() with:
      | material.reflective | transform            |
      |  1                  | translation(0, 1, 0) |
    And upper is added to w
    And r ← ray(point(0, 0, 0), vector(0, 1, 0))
   Then color_at(w, r) should terminate successfully

Scenario: The reflected color at the maximum recursive depth
  Given w ← default_world()
    And shape ← plane() with:
      | material.reflective | transform             |
      | 0.5                 | translation(0, -1, 0) |
    And shape is added to w
    And r ← ray(point(0, 0, -3), vector(0, -√2/2, √2/2))
    And i ← intersection(√2, shape)
   When comps ← prepare_computations(i, r)
    And color ← reflected_color(w, comps, 0)
   Then color = color(0, 0, 0)

Scenario: The refracted color with an opaque surface
  Given w ← default_world()
    And shape ← the first object in w
    And r ← ray(point(0, 0, -5), vector(0, 0, 1))
    And xs ← intersections(4:shape, 6:shape)
   When comps ← prepare_computations(xs[0], r, xs)
    And c ← refracted_color(w, comps, 5)
   Then c = color(0, 0, 0)

Scenario: The refracted color at the maximum recursive depth
  Given w ← default_world()
    And shape ← the first object in w
    And shape has:
      | material.transparency | material.refractive_index |
      | 1.0                   | 1.5                       |
    And r ← ray(point(0, 0, -5), vector(0, 0, 1))
    And xs ← intersections(4:shape, 6:shape)
   When comps ← prepare_computations(xs[0], r, xs)
    And c ← refracted_color(w, comps, 0)
   Then c = color(0, 0, 0)

Scenario: The refracted color under total internal reflection
  Given w ← default_world()
    And shape ← the first object in w
    And shape has:
      | material.transparency | material.refractive_index |
      | 1.0                   | 1.5                       |
    And r ← ray(point(0, 0, √2/2), vector(0, 1, 0))
    And xs ← intersections(-√2/2:shape, √2/2:shape)
  # NOTE: this time you're inside the sphere, so you need
  # to look at the second intersection, xs[1], not xs[0]
   When comps ← prepare_computations(xs[1], r, xs)
    And c ← refracted_color(w, comps, 5)
   Then c = color(0, 0, 0)

Scenario: The refracted color with a refracted ray
  Given w ← default_world()
    And A ← the first object in w
    And A has:
      | material.ambient | material.pattern |
      | 1.0              | test_pattern()   |
    And B ← the second object in w
    And B has:
      | material.transparency | material.refractive_index |
      | 1.0                   | 1.5                       |
    And r ← ray(point(0, 0, 0.1), vector(0, 1, 0))
    And xs ← intersections(-0.9899:A, -0.4899:B, 0.4899:B, 0.9899:A)
   When comps ← prepare_computations(xs[2], r, xs)
    And c ← refracted_color(w, comps, 5)
   Then c = color(0, 0.99888, 0.04725)

Scenario: shade_hit() with a transparent material
  Given w ← default_world()
    And floor ← plane() with:
      | transform             | material.transparency | material.refractive_index |
      | translation(0, -1, 0) | 0.5                   | 1.5                       |
    And floor is added to w
    And ball ← sphere() with:
      | material.color | material.ambient | transform                  |
      | (1, 0, 0)      | 0.5              | translation(0, -3.5, -0.5) |      
    And ball is added to w
    And r ← ray(point(0, 0, -3), vector(0, -√2/2, √2/2))
    And xs ← intersections(√2:floor)
   When comps ← prepare_computations(xs[0], r, xs)
    And color ← shade_hit(w, comps, 5)
   Then color = color(0.93642, 0.68642, 0.68642)

Scenario: shade_hit() with a reflective, transparent material
  Given w ← default_world()
    And r ← ray(point(0, 0, -3), vector(0, -√2/2, √2/2))
    And floor ← plane() with:
      | transform             | material.reflective | material.transparency | material.refractive_index |
      | translation(0, -1, 0) | 0.5                 | 0.5                   | 1.5                       |
    And floor is added to w
    And ball ← sphere() with:
      | material.color | material.ambient | transform                  |
      | (1, 0, 0)      | 0.5              | translation(0, -3.5, -0.5) |
    And ball is added to w
    And xs ← intersections(√2:floor)
   When comps ← prepare_computations(xs[0], r, xs)
    And color ← shade_hit(w, comps, 5)
   Then color = color(0.93391, 0.69643, 0.69243)
