﻿using RayTracer.Shapes;
using RayTracer.Tests.Transforms;
using RayTracer.Tracing;
using TechTalk.SpecFlow;

namespace RayTracer.Tests.Chapter11;

public sealed class WorldSteps : CommonStepBase
{
    public WorldSteps(ScenarioContext context) : base(context) { }

    [Given(@"(\w+) ← plane\(\) with:")]
    public void GivenSphereWith(string name, Plane plane)
    {
        _context[name] = plane;
    }

    [Given(@"(\w+) has:")]
    public void GivenShapeHas(string name, Table table)
    {
        ShapeTransforms.ApplyTable(Get<IShape>(name), table);
    }

    [When(@"(\w+) ← reflected_color\((\w+), (\w+)\)")]
    public void WhenReflectedColor(string name, string world, string comps)
    {
        _context[name] = Get<World>(world).ReflectedColor(Get<ComputationData>(comps), World.MaxDepth);
    }

    [When(@"(\w+) ← reflected_color\((\w+), (\w+), (\d+)\)")]
    public void WhenReflectedColor(string name, string world, string comps, int remaining)
    {
        _context[name] = Get<World>(world).ReflectedColor(Get<ComputationData>(comps), remaining);
    }

    [When(@"(\w+) ← refracted_color\((\w+), (\w+)\)")]
    public void WhenRefractedColor(string name, string world, string comps)
    {
        _context[name] = Get<World>(world).RefractedColor(Get<ComputationData>(comps), World.MaxDepth);
    }

    [When(@"(\w+) ← refracted_color\((\w+), (\w+), (\d+)\)")]
    public void WhenRefractedColor(string name, string world, string comps, int remaining)
    {
        _context[name] = Get<World>(world).RefractedColor(Get<ComputationData>(comps), remaining);
    }

    [When(@"(\w+) ← shade_hit\((\w+), (\w+), (\d+)\)")]
    public void WhenShadeHit(string name, string world, string comps, int remaining)
    {
        _context[name] = Get<World>(world).ShadeHit(Get<ComputationData>(comps), remaining);
    }

    [Then(@"color_at\((\w+), (\w+)\) should terminate successfully")]
    public void ThenColorAtShouldTerminateSuccessfully(string world, string ray)
    {
        Get<World>(world).ColorAt(Get<Ray>(ray));
    }

}
