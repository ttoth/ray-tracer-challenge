﻿using RayTracer.Math;
using RayTracer.Shapes;
using RayTracer.Tracing;
using TechTalk.SpecFlow;

namespace RayTracer.Tests.Chapter12;

public sealed class CubeSteps : CommonStepBase
{
    public CubeSteps(ScenarioContext context) : base(context) { }

    [Given(@"(\w+) ← cube\(\)")]
    public void GivenCube(string name)
    {
        _context[name] = new Cube();
    }

    [When(@"(\w+) ← local_intersect\((\w+), (\w+)\)")]
    public void WhenLocalIntersect(string name, string shape, string ray)
    {
        _context[name] = Get<Shape>(shape).LocalIntersect(Get<Ray>(ray));
    }

    [When(@"(\w+) ← local_normal_at\((\w+), (\w+)\)")]
    public void WhenLocalNormalAt(string name, string shape, string point)
    {
        _context[name] = Get<Shape>(shape).LocalNormalAt(Get<Tuple>(point));
    }
}
