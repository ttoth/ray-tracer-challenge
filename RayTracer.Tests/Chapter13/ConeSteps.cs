﻿using RayTracer.Shapes;
using TechTalk.SpecFlow;

namespace RayTracer.Tests.Chapter13;

public class ConeSteps : CommonStepBase
{
    public ConeSteps(ScenarioContext context) : base(context) { }

    [Given(@"(\w+) ← cone\(\)")]
    public void GivenCone(string name)
    {
        _context[name] = new Cone();
    }
}
