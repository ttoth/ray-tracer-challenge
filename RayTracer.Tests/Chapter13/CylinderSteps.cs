﻿using RayTracer.Math;
using RayTracer.Shapes;
using TechTalk.SpecFlow;

namespace RayTracer.Tests.Chapter13;

public class CylinderSteps : CommonStepBase
{
    public CylinderSteps(ScenarioContext context) : base(context) { }

    [Given(@"(\w+) ← cylinder\(\)")]
    public void GivenACylinder(string name)
    {
        _context[name] = new Cylinder();
    }

    [When(@"(\w+) ← local_normal_at\((\w+), (\w+\(.*\))\)")]
    public void WhenLocalNormalAt(string name, string shape, Tuple point)
    {
        _context[name] = Get<Shape>(shape).LocalNormalAt(point);
    }
}
