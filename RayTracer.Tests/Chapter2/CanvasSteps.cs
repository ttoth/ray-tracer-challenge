﻿using RayTracer.Math;
using TechTalk.SpecFlow;
using Xunit;
using static System.Environment;

namespace RayTracer.Tests.Chapter2;

public class CanvasSteps : CommonStepBase
{
    public CanvasSteps(ScenarioContext context) : base(context) { }

    [Given(@"(\w+) ← canvas\((\d+), (\d+)\)")]
    public void GivenACanvas(string name, int width, int height)
    {
        _context[name] = new Canvas(width, height);
    }

    [When(@"write_pixel\((\w+), (.*), (.*), (\w+)\)")]
    public void WhenWritePixel(string name, int x, int y, string value)
    {
        var canvas = Get<Canvas>(name);
        var color = Get<Tuple>(value);

        canvas.WritePixel(x, y, color);
    }

    [When(@"(\w+) ← canvas_to_ppm\((\w+)\)")]
    public void WhenCanvasIsConvertedToPPM(string name, string canvas)
    {
        _context[name] = Get<Canvas>(canvas).ToPPM();
    }

    [When(@"every pixel of (\w+) is set to (.*)")]
    public void WhenEveryPixelOfCIsSetToColor(string name, Tuple color)
    {
        var canvas = Get<Canvas>(name);
        for (int x = 0; x < canvas.Width; ++x)
            for (int y = 0; y < canvas.Height; ++y)
                canvas.WritePixel(x, y, color);
    }

    [Then(@"every pixel of (\w+) is (.*)")]
    public void ThenEveryPixelIs(string name, Tuple value)
    {
        var canvas = Get<Canvas>(name);
        for (int x = 0; x < canvas.Width; ++x)
            for (int y = 0; y < canvas.Height; ++y)
                Assert.Equal(value, canvas.PixelAt(x, y));
    }

    [Then(@"pixel_at\((\w+), (.*), (.*)\) = (\w+)")]
    public void ThenPixelAtEquals(string name, int x, int y, string value)
    {
        var canvas = Get<Canvas>(name);
        var color = Get<Tuple>(value);

        var result = canvas.PixelAt(x, y);
        Assert.Equal(color, result);
    }

    [Then(@"pixel_at\((\w+), (.*), (.*)\) = (\w+\(.*\))")]
    public void ThenPixelAtEquals(string name, int x, int y, Tuple expected)
    {
        var canvas = Get<Canvas>(name);

        var result = canvas.PixelAt(x, y);
        Assert.Equal(expected, result);
    }

    [Then(@"lines (\d+)-(\d+) of (\w+) are")]
    public void ThenLinesOfPpmAre(int from, int to, string name, string multilineText)
    {
        var content = (string)_context[name];

        int fromIndex = 0, toIndex = 0, newLineLength = NewLine.Length;
        while (--from > 0) fromIndex = content.IndexOf(NewLine, fromIndex) + newLineLength;
        while (to-- > 0) toIndex = content.IndexOf(NewLine, toIndex + newLineLength);

        Assert.Equal(multilineText, content[fromIndex..toIndex], ignoreLineEndingDifferences: true);
    }

    [Then(@"(\w+) ends with a newline character")]
    public void ThenPpmEndsWithANewlineCharacter(string name)
    {
        var content = (string)_context[name];
        Assert.EndsWith(NewLine, content);
    }
}
