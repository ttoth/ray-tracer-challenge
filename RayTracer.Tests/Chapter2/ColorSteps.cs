﻿using RayTracer.Math;
using TechTalk.SpecFlow;
using Xunit;

namespace RayTracer.Tests.Chapter2;

public sealed class ColorSteps : CommonStepBase
{
    public ColorSteps(ScenarioContext context) : base(context) { }

    [Then(@"([a-z]\w*) \* ([a-z]\w*) = (.*)")]
    public void ThenMultipliedByTupleEquals(string name, string multiplier, Tuple expected)
    {
        var result = Get<Tuple>(name) * Get<Tuple>(multiplier);
        Assert.Equal(expected, result);
    }
}
