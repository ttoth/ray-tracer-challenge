﻿using RayTracer.Math;
using RayTracer.Math.Matrices;
using TechTalk.SpecFlow;
using Xunit;

namespace RayTracer.Tests.Chapter3;

public sealed class MatricesSteps : CommonStepBase
{
    public MatricesSteps(ScenarioContext context) : base(context) { }

    [Given(@"the following 4x4 matrix (\w+):")]
    [Given(@"the following matrix (\w+):")]
    public void GivenTheFollowing4x4Matrix(string name, Matrix44 matrix)
    {
        _context[name] = matrix;
    }

    [Given(@"the following 3x3 matrix (\w+):")]
    public void GivenTheFollowing3x3Matrix(string name, Matrix33 matrix)
    {
        _context[name] = matrix;
    }

    [Given(@"the following 2x2 matrix (\w+):")]
    public void GivenTheFollowing2x2Matrix(string name, Matrix22 matrix)
    {
        _context[name] = matrix;
    }

    [Given(@"(.*) ← submatrix\((.*), (.*), (.*)\)")]
    public void GivenSubmatrix(string result, string name, int row, int column)
    {
        _context[result] = Get<Matrix>(name).SubMatrix(row, column);
    }

    [Given(@"(.*) ← ([A-Z]\w*) \* ([A-Z]\w*)")]
    public void GivenTwoMartixMultiplied(string result, string a, string b)
    {
        _context[result] = Matrix4x4(a) * Matrix4x4(b);
    }

    [Given(@"(.*) ← inverse\((.*)\)")]
    public void GivenInverseOfAMatrix(string result, string a)
    {
        _context[result] = Get<Matrix>(a).Inverse();
    }

    [Then(@"(.*)\[(\d+),(\d+)] = (.*)")]
    public void ThenMatrixCellEquals(string name, int index1, int index2, double value)
    {
        var matrix = Get<Matrix>(name);

        Assert.Equal(value, matrix[index1, index2]);
    }

    [Then(@"(\w+) is the following 4x4 matrix:")]
    public void ThenABIsTheFollowingMatrix(string name, Matrix44 expected)
    {
        Assert.Equal(expected, Get<Matrix>(name));
    }

    [Then(@"(.*) \* (.*) is the following 4x4 matrix:")]
    public void ThenABIsTheFollowingMatrix(string a, string b, Matrix44 expected)
    {
        var matrixA = Matrix4x4(a);
        var matrixB = Matrix4x4(b);
        Assert.Equal(expected, matrixA * matrixB);
    }

    [Then(@"([A-Z]\w*) \* ([a-z]\w*) = (\w+\(.*\))")]
    public void ThenMultipliedByTupleEquals(string name, string multiplier, Tuple expected)
    {
        var result = Matrix4x4(name) * Get<Tuple>(multiplier);
        Assert.Equal(expected, result);
    }

    [Then(@"([A-Z]\w*) \* ([A-Z]\w*) = (.*)")]
    public void ThenMultipliedByMatrixEquals(string a, string b, string expected)
    {
        Assert.Equal(Matrix4x4(expected), Matrix4x4(a) * Matrix4x4(b));
    }

    [Then(@"([A-Z]\w*) \* ([a-z]\w*) = ([a-z]\w*)")]
    public void ThenMultipliedByTupleEquals(string a, string b, string expected)
    {
        Assert.Equal(Get<Tuple>(expected), Matrix4x4(a) * Get<Tuple>(b));
    }

    [Then(@"transpose\((.*)\) is the following matrix:")]
    public void ThenTransposeMatrixIsTheFollowingMatrix(string name, Matrix44 expected)
    {
        var result = Matrix4x4(name).Transpose();
        Assert.Equal(expected, result);
    }

    [Then(@"determinant\((.*)\) = (.*)")]
    public void ThenDeterminantEquals(string name, double expected)
    {
        var result = Get<Matrix>(name).Determinant;
        Assert.Equal(expected, result);
    }

    [Then(@"submatrix\((.*), (.*), (.*)\) is the following 2x2 matrix:")]
    public void ThenSubmatrixIsTheFollowingMatrix(string name, int row, int column, Matrix22 expected)
    {
        var result = Get<Matrix33>(name).SubMatrix(row, column);
        Assert.Equal(expected, result);
    }

    [Then(@"submatrix\((.*), (.*), (.*)\) is the following 3x3 matrix:")]
    public void ThenSubmatrixIsTheFollowingMatrix(string name, int row, int column, Matrix33 expected)
    {
        var result = Matrix4x4(name).SubMatrix(row, column);
        Assert.Equal(expected, result);
    }

    [Then(@"minor\((.*), (.*), (.*)\) = (.*)")]
    public void ThenMinorEquals(string name, int row, int column, double expected)
    {
        var result = Get<Matrix>(name).Minor(row, column);
        Assert.Equal(expected, result);
    }

    [Then(@"cofactor\((.*), (.*), (.*)\) = (.*)")]
    public void ThenCofactorEquals(string name, int row, int column, double expected)
    {
        var result = Get<Matrix>(name).Cofactor(row, column);
        Assert.Equal(expected, result);
    }

    [Then("(.*) is invertible")]
    public void ThenMatrixIsInvertible(string name)
    {
        Assert.True(Get<Matrix>(name).IsInvertible);
    }

    [Then("(.*) is not invertible")]
    public void ThenMatrixIsNotInvertible(string name)
    {
        Assert.False(Get<Matrix>(name).IsInvertible);
    }

    [Then(@"inverse\((.*)\) is the following 4x4 matrix:")]
    public void ThenInverseIsTheFollowingMatrix(string name, Matrix44 expected)
    {
        var result = Get<Matrix>(name).Inverse();
        Assert.Equal(expected, result);
    }

    [Then(@"(.*) \* inverse\((.*)\) = (.*)")]
    public void ThenMatrixMultipliedByInverseEquals(string a, string b, string expected)
    {
        Assert.Equal(Get<Matrix>(expected), Matrix4x4(a) * Matrix4x4(b).Inverse());
    }
}
