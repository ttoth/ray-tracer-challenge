﻿using RayTracer.Math;
using RayTracer.Math.Matrices;
using TechTalk.SpecFlow;

namespace RayTracer.Tests.Chapter4;

public class TransformationSteps : CommonStepBase
{
    public TransformationSteps(ScenarioContext context) : base(context) { }

    [Given(@"(\w+) ← ((?>translation|scaling|rotation_[xyz]|shearing)\([^\)]*\))")]
    public void GivenTranslationMatrix(string name, Matrix44 matrix)
    {
        _context[name] = matrix;
    }

    [When(@"(\w+) ← (\w+) \* (\w+)")]
    public void WhenMatrixMultipliedByTuple(string name, string a, string b)
    {
        _context[name] = Matrix4x4(a) * Get<Tuple>(b);
    }

    [When(@"(\w+) ← (\w+) \* (\w+) \* (\w+)")]
    public void When3MatricesMultiplied(string name, string a, string b, string c)
    {
        _context[name] = Matrix4x4(a) * Matrix4x4(b) * Matrix4x4(c);
    }

}
