﻿Feature: Matrix Transformations

Scenario: Multiplying by a translation matrix
  Given T ← translation(5, -3, 2)
    And p ← point(-3, 4, 5)
   Then T * p = point(2, 1, 7)

Scenario: Multiplying by the inverse of a translation matrix
  Given T ← translation(5, -3, 2)
    And Inv ← inverse(T)
    And p ← point(-3, 4, 5)
   Then Inv * p = point(-8, 7, 3)

Scenario: Translation does not affect vectors
  Given T ← translation(5, -3, 2)
    And v ← vector(-3, 4, 5)
   Then T * v = v

Scenario: A scaling matrix applied to a point
  Given T ← scaling(2, 3, 4)
    And p ← point(-4, 6, 8)
   Then T * p = point(-8, 18, 32)

Scenario: A scaling matrix applied to a vector
  Given T ← scaling(2, 3, 4)
    And v ← vector(-4, 6, 8)
   Then T * v = vector(-8, 18, 32)

Scenario: Multiplying by the inverse of a scaling matrix
  Given T ← scaling(2, 3, 4)
    And Inv ← inverse(T)
    And v ← vector(-4, 6, 8)
   Then Inv * v = vector(-2, 2, 2)

Scenario: Reflection is scaling by a negative value
  Given T ← scaling(-1, 1, 1)
    And p ← point(2, 3, 4)
   Then T * p = point(-2, 3, 4)

Scenario: Rotating a point around the x axis
  Given p ← point(0, 1, 0)
    And HQ ← rotation_x(π/4)
    And FQ ← rotation_x(π/2)
   Then HQ * p = point(0, √2/2, √2/2)
    And FQ * p = point(0, 0, 1)

Scenario: The inverse of an x-rotation rotates in the opposite direction
  Given p ← point(0, 1, 0)
    And HQ ← rotation_x(π/4)
    And Inv ← inverse(HQ)
   Then Inv * p = point(0, √2/2, -√2/2)

Scenario: Rotating a point around the y axis
  Given p ← point(0, 0, 1)
    And HQ ← rotation_y(π/4)
    And FQ ← rotation_y(π/2)
   Then HQ * p = point(√2/2, 0, √2/2)
    And FQ * p = point(1, 0, 0)

Scenario: Rotating a point around the z axis
  Given p ← point(0, 1, 0)
    And HQ ← rotation_z(π/4)
    And FQ ← rotation_z(π/2)
   Then HQ * p = point(-√2/2, √2/2, 0)
    And FQ * p = point(-1, 0, 0)

Scenario: A shearing transformation moves x in proportion to y
  Given T ← shearing(1, 0, 0, 0, 0, 0)
    And p ← point(2, 3, 4)
   Then T * p = point(5, 3, 4)

Scenario: A shearing transformation moves x in proportion to z
  Given T ← shearing(0, 1, 0, 0, 0, 0)
    And p ← point(2, 3, 4)
   Then T * p = point(6, 3, 4)

Scenario: A shearing transformation moves y in proportion to x
  Given T ← shearing(0, 0, 1, 0, 0, 0)
    And p ← point(2, 3, 4)
   Then T * p = point(2, 5, 4)

Scenario: A shearing transformation moves y in proportion to z
  Given T ← shearing(0, 0, 0, 1, 0, 0)
    And p ← point(2, 3, 4)
   Then T * p = point(2, 7, 4)

Scenario: A shearing transformation moves z in proportion to x
  Given T ← shearing(0, 0, 0, 0, 1, 0)
    And p ← point(2, 3, 4)
   Then T * p = point(2, 3, 6)

Scenario: A shearing transformation moves z in proportion to y
  Given T ← shearing(0, 0, 0, 0, 0, 1)
    And p ← point(2, 3, 4)
   Then T * p = point(2, 3, 7)

Scenario: Individual transformations are applied in sequence
  Given p ← point(1, 0, 1)
    And A ← rotation_x(π/2)
    And B ← scaling(5, 5, 5)
    And C ← translation(10, 5, 7)
  # apply rotation first
   When p2 ← A * p
   Then p2 = point(1, -1, 0)
   # then apply scaling
   When p3 ← B * p2
   Then p3 = point(5, -5, 0)
   # then apply translation
   When p4 ← C * p3
   Then p4 = point(15, 0, 7)

Scenario: Chained transformations must be applied in reverse order
  Given p ← point(1, 0, 1)
    And A ← rotation_x(π/2)
    And B ← scaling(5, 5, 5)
    And C ← translation(10, 5, 7)
   When T ← C * B * A
   Then T * p = point(15, 0, 7)
