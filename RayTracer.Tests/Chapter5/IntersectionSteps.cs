﻿using RayTracer.Shapes;
using RayTracer.Tests.Extensions;
using RayTracer.Tracing;
using TechTalk.SpecFlow;
using Xunit;

namespace RayTracer.Tests.Chapter5;

public sealed class IntersectionSteps : CommonStepBase
{
    public IntersectionSteps(ScenarioContext context) : base(context) { }

    [Given(@"(\w+) ← intersection\((.*), (\w+)\)")]
    [When(@"(\w+) ← intersection\((.*), (\w+)\)")]
    public void GivenIntersection(string name, double t, string obj)
    {
        _context[name] = new Intersection(t, Get<IShape>(obj));
    }

    [Given(@"(\w+) ← intersections\((\w+), (\w+)\)")]
    [When(@"(\w+) ← intersections\((\w+), (\w+)\)")]
    public void GivenIntersections(string name, string i1, string i2)
    {
        _context[name] = new Intersections(Get<Intersection>(i1), Get<Intersection>(i2));
    }

    [Given(@"(\w+) ← intersections\((\w+), (\w+), (\w+)\)")]
    [When(@"(\w+) ← intersections\((\w+), (\w+), (\w+)\)")]
    public void GivenIntersections(string name, string i1, string i2, string i3)
    {
        _context[name] = new Intersections(Get<Intersection>(i1), Get<Intersection>(i2), Get<Intersection>(i3));
    }

    [Given(@"(\w+) ← intersections\((\w+), (\w+), (\w+), (\w+)\)")]
    [When(@"(\w+) ← intersections\((\w+), (\w+), (\w+), (\w+)\)")]
    public void GivenIntersections(string name, string i1, string i2, string i3, string i4)
    {
        _context[name] = new Intersections(Get<Intersection>(i1), Get<Intersection>(i2), Get<Intersection>(i3), Get<Intersection>(i4));
    }

    [Then(@"(\w+)\[(\d+)]\.(\w+) = (-?[\d\.]+)")]
    public void ThenElementTEquals(string name, int index, string prop, double expected)
    {
        Assert.Equal(expected, (double)Get<Intersections>(name)[index].PropertyValue(prop)!, 5);
    }

    [Then(@"(\w+)\[(\d+)]\.(\w+) = ([a-z]\w*)")]
    public void ThenElementPropertyEquals(string name, int index, string prop, string expected)
    {
        Assert.Equal(Get<object>(expected), Get<Intersections>(name)[index].PropertyValue(prop));
    }

    [When(@"(\w+) ← hit\((\w+)\)")]
    public void WhenHit(string name, string i)
    {
        _context[name] = Get<Intersections>(i).Hit();
    }
}
