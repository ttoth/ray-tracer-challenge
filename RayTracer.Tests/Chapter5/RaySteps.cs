﻿using RayTracer.Math;
using RayTracer.Math.Matrices;
using RayTracer.Tracing;
using TechTalk.SpecFlow;
using Xunit;

namespace RayTracer.Tests.Chapter5;

public sealed class RaySteps : CommonStepBase
{
    public RaySteps(ScenarioContext context) : base(context) { }

    [When(@"(\w+) ← ray\((\w+), (\w+)\)")]
    public void WhenCreatingARay(string name, string origin, string direction)
    {
        _context[name] = new Ray(Get<Tuple>(origin), Get<Tuple>(direction));
    }

    [Given(@"(\w+) ← ray\((\w+\(.*\)), (\w+\(.*\))\)")]
    public void WhenCreatingARay(string name, Tuple origin, Tuple direction)
    {
        _context[name] = new Ray(origin, direction);
    }

    [Given(@"(\w+) ← ray\((\w+\(.*\)), (\w+)\)")]
    public void WhenCreatingARay(string name, Tuple origin, string direction)
    {
        _context[name] = new Ray(origin, Get<Tuple>(direction));
    }

    [When(@"(\w+) ← transform\((\w+), (\w+)\)")]
    public void WhenTransformed(string name, string ray, string matrix)
    {
        _context[name] = Get<Ray>(ray).Transform(Get<Matrix44>(matrix));
    }

    [Then(@"position\((\w+), (.*)\) = (.*)")]
    public void ThenPositionRPoint(string name, double t, Tuple expected)
    {
        Assert.Equal(expected, Get<Ray>(name).Position(t));
    }
}
