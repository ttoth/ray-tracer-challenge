﻿using RayTracer.Math.Matrices;
using RayTracer.Shapes;
using RayTracer.Tracing;
using TechTalk.SpecFlow;
using Xunit;

namespace RayTracer.Tests.Chapter5;

public sealed class SphereSteps : CommonStepBase
{
    public SphereSteps(ScenarioContext context) : base(context) { }

    [Given(@"(\w+) ← sphere\(\)")]
    public void GivenSphere(string name)
    {
        _context[name] = new Sphere();
    }

    [When(@"(\w+) ← intersect\((\w+), (\w+)\)")]
    public void WhenSphereIntersectsRay(string name, string sphere, string ray)
    {
        _context[name] = Get<IShape>(sphere).Intersect(Get<Ray>(ray));
    }

    [Given(@"set_transform\((\w+), (\w+)\)")]
    [When(@"set_transform\((\w+), (\w+)\)")]
    public void WhenSetTransform(string sphere, string matrix)
    {
        Get<IShape>(sphere).Transform = Get<Matrix44>(matrix);
    }

    [Given(@"set_transform\((\w+), (\w+\(.*\))\)")]
    [When(@"set_transform\((\w+), (\w+\(.*\))\)")]
    public void WhenSetTransform(string sphere, Matrix44 matrix)
    {
        Get<IShape>(sphere).Transform = matrix;
    }

    [Then(@"(\w+)\[(\d+)] = (.*)")]
    public void ThenElementEquals(string name, int index, double expected)
    {
        Assert.Equal(expected, Get<Intersections>(name)[index].T);
    }
}
