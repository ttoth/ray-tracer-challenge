﻿using RayTracer.Lights;
using RayTracer.Math;
using TechTalk.SpecFlow;

namespace RayTracer.Tests.Chapter6;

public sealed class LightSteps : CommonStepBase
{
    public LightSteps(ScenarioContext context) : base(context) { }

    [Given(@"(\w+) ← point_light\((\w+), (\w+)\)")]
    [When(@"(\w+) ← point_light\((\w+), (\w+)\)")]
    public void GivenPointLight(string name, string pos, string color)
    {
        _context[name] = new PointLight(Get<Tuple>(pos), Get<Tuple>(color));
    }

    [Given(@"(\w+) ← point_light\((\w+\(.*\)), (\w+\(.*\))\)")]
    public void GivenPointLight(string name, Tuple pos, Tuple color)
    {
        _context[name] = new PointLight(pos, color);
    }
}
