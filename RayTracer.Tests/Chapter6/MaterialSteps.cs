﻿using RayTracer.Lights;
using RayTracer.Materials;
using RayTracer.Math;
using RayTracer.Shapes;
using TechTalk.SpecFlow;
using Xunit;

namespace RayTracer.Tests.Chapter6;

public sealed class MaterialSteps : CommonStepBase
{
    public MaterialSteps(ScenarioContext context) : base(context) { }

    [Given(@"(\w) ← material\(\)")]
    public void GivenMaterial(string name)
    {
        _context[name] = new Material();
    }

    [When(@"(\w+) ← lighting\((\w+), (\w+), (\w+), (\w+), (\w+)\)")]
    public void WhenLighting(string name, string m, string light, string position, string eye, string normal)
    {
        _context[name] = new Sphere { Material = Get<Material>(m) }.Lighting(Get<PointLight>(light), Get<Tuple>(position), Get<Tuple>(eye), Get<Tuple>(normal), false);
    }

    [Then(@"(\w+) = material\(\)")]
    public void ThenMaterialIsDefault(string name)
    {
        Assert.Equal(new Material(), _context.Get<Material>(name));
    }
}
