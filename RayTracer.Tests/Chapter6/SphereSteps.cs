﻿using RayTracer.Materials;
using RayTracer.Math;
using RayTracer.Math.Matrices;
using RayTracer.Shapes;
using TechTalk.SpecFlow;
using Xunit;

namespace RayTracer.Tests.Chapter6;

public sealed class SphereSteps : CommonStepBase
{
    public SphereSteps(ScenarioContext context) : base(context) { }

    [Given(@"(\w+) ← (.*) \* (.*)")]
    public void GivenMatricesMultiplied(string name, Matrix44 a, Matrix44 b)
    {
        _context[name] = a * b;
    }

    [When(@"(\w+) ← normal_at\((\w+), (.*)\)")]
    public void WhenNormalAtPoint(string name, string shape, Tuple point)
    {
        _context[name] = Get<IShape>(shape).NormalAt(point);
    }

    [When(@"(\w+) ← (\w+)\.material")]
    public void WhenMaterialOfShape(string name, string shape)
    {
        _context[name] = Get<IShape>(shape).Material;
    }

    [When(@"(\w+)\.material ← (\w+)")]
    public void WhenMaterial(string name, string material)
    {
        Get<IShape>(name).Material = Get<Material>(material);
    }

    [Then(@"(\w+) = normalize\((\w+)\)")]
    public void ThenEqualsNormalize(string name, string vector)
    {
        Assert.Equal(Get<Tuple>(vector).Normalize(), Get<Tuple>(name));
    }
}
