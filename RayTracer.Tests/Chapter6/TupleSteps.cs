﻿using RayTracer.Math;
using TechTalk.SpecFlow;

namespace RayTracer.Tests.Chapter6;

public sealed class TupleSteps : CommonStepBase
{
    public TupleSteps(ScenarioContext context) : base(context) { }

    [When(@"(\w+) ← reflect\((\w+), (\w+)\)")]
    public void WhenReflectVector(string name, string vector, string normal)
    {
        _context[name] = Get<Tuple>(vector).Reflect(Get<Tuple>(normal));
    }

}
