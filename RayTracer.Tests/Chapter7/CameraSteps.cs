﻿using RayTracer.Math;
using RayTracer.Math.Matrices;
using TechTalk.SpecFlow;

namespace RayTracer.Tests.Chapter7;

public sealed class CameraSteps : CommonStepBase
{
    public CameraSteps(ScenarioContext context) : base(context) { }

    [Given(@"(\w+) ← camera\((.*), (.*), (.*)\)")]
    public void WhenC_CameraHsizeVsizeField_Of_View(string name, int hsize, int vsize, double fov)
    {
        _context[name] = new Camera(hsize, vsize, fov);
    }

    [When(@"(\w+) ← camera\((\w+), (\w+), (\w+)\)")]
    public void WhenC_CameraHsizeVsizeField_Of_View(string name, string hsize, string vsize, string fov)
    {
        _context[name] = new Camera((int)Get<double>(hsize), (int)Get<double>(vsize), Get<double>(fov));
    }

    [When(@"(\w+)\.transform ← (.*) \* (.*)")]
    public void WhenTransformSet(string name, Matrix44 a, Matrix44 b)
    {
        Get<Camera>(name).Transform = a * b;
    }

    [Given(@"(\w+)\.transform ← view_transform\((\w+), (\w+), (\w+)\)")]
    public void WhenTransformSetToViewTransform(string name, string from, string to, string up)
    {
        Get<Camera>(name).Transform = Transformation.ViewTransform(Get<Tuple>(from), Get<Tuple>(to), Get<Tuple>(up));
    }

    [When(@"(\w+) ← ray_for_pixel\((\w+), (.*), (.*)\)")]
    public void WhenR_Ray_For_PixelC(string name, string camera, int px, int py)
    {
        _context[name] = Get<Camera>(camera).RayForPixel(px, py);
    }

    [When(@"(\w+) ← render\((\w+), (\w+)\)")]
    public void WhenRenderImage(string name, string camera, string world)
    {
        _context[name] = Get<Camera>(camera).Render(Get<World>(world));
    }

}
