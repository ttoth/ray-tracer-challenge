﻿using RayTracer.Tracing;
using TechTalk.SpecFlow;
using Xunit;

namespace RayTracer.Tests.Chapter7;

public sealed class IntersectionSteps : CommonStepBase
{
    public IntersectionSteps(ScenarioContext context) : base(context) { }

    [When(@"(\w+) ← prepare_computations\((\w+), (\w+)\)")]
    public void WhenPrepareComputations(string name, string i, string r)
    {
        var intersection = Get<Intersection>(i);
        _context[name] = intersection.PrepareComputations(Get<Ray>(r), new Intersections(intersection));
    }

    [Then(@"(\w+)\.t = (\w+)\.t")]
    public void ThenTEquals(string name, string expected)
    {
        Assert.Equal(Get<Intersection>(expected).T, Get<ComputationData>(name).T);
    }

    [Then(@"(\w+)\.object = (\w+)\.object")]
    public void ThenObjectEquals(string name, string expected)
    {
        Assert.Equal(Get<Intersection>(expected).Object, Get<ComputationData>(name).Object);
    }
}
