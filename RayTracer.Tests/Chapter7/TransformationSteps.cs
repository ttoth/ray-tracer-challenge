﻿using RayTracer.Math;
using RayTracer.Math.Matrices;
using TechTalk.SpecFlow;
using Xunit;

namespace RayTracer.Tests.Chapter7;

public sealed class TransformationSteps : CommonStepBase
{
    public TransformationSteps(ScenarioContext context) : base(context) { }

    [When(@"(\w+) ← view_transform\((\w+), (\w+), (\w+)\)")]
    public void WhenViewTransformGiven(string name, string from, string to, string up)
    {
        _context[name] = Transformation.ViewTransform(Get<Tuple>(from), Get<Tuple>(to), Get<Tuple>(up));
    }

    [Then(@"(\w+) = ((?>translation|scaling|rotation_[xyz]|shearing)\([^\)]*\))")]
    public void GivenTranslationMatrix(string name, Matrix44 expected)
    {
        Assert.Equal(expected, Get<Matrix>(name));
    }
}
