﻿using RayTracer.Lights;
using RayTracer.Math;
using RayTracer.Math.Matrices;
using RayTracer.Shapes;
using RayTracer.Tracing;
using System.Linq;
using TechTalk.SpecFlow;
using Xunit;

namespace RayTracer.Tests.Chapter7;

public sealed class WorldSteps : CommonStepBase
{
    public WorldSteps(ScenarioContext context) : base(context) { }

    [Given(@"(\w+) ← world\(\)")]
    public void GivenWorld(string name)
    {
        _context[name] = new World();
    }

    [Given(@"(\w+) ← sphere\(\) with:")]
    public void GivenSphereWith(string name, Sphere sphere)
    {
        _context[name] = sphere;
    }

    [Given(@"(\w+) ← default_world\(\)")]
    [When(@"(\w+) ← default_world\(\)")]
    public void GivenDefaultWorld(string name)
    {
        var world = new World();
        world.Lights.Add(new PointLight(Tuple.Point(-10, 10, -10), Tuple.Color(1, 1, 1)));

        world.Objects.Add(new Sphere
        {
            Material = new Materials.Material
            {
                Color = Tuple.Color(0.8, 1, 0.6),
                Diffuse = 0.7,
                Specular = 0.2
            }
        });

        world.Objects.Add(new Sphere
        {
            Transform = Transformation.Scaling(0.5, 0.5, 0.5)
        });

        _context[name] = world;
    }

    [Given(@"(\w+)\.light ← point_light\((\w+\(.*\)), (\w+\(.*\))\)")]
    public void GivenWorldLight(string name, Tuple pos, Tuple color)
    {
        var world = Get<World>(name);
        var light = new PointLight(pos, color);

        if (world.Lights.Any())
        {
            world.Lights[0] = light;
        }
        else
        {
            world.Lights.Add(light);
        }
    }

    [Given(@"(\w+) ← the (\w+) object in (\w+)")]
    public void GivenTheFirstObject(string name, int index, string world)
    {
        _context[name] = Get<World>(world).Objects[index];
    }

    [Given(@"(\w+)\.material\.ambient ← (.*)")]
    public void GivenMaterialAmbient(string name, double ambient)
    {
        var shape = Get<IShape>(name);
        shape.Material = shape.Material with { Ambient = ambient };
    }

    [When(@"(\w+) ← intersect_world\((\w+), (\w+)\)")]
    public void WhenIntersectWorl(string name, string world, string ray)
    {
        _context[name] = Get<World>(world).Intersect(Get<Ray>(ray));
    }

    [When(@"(\w+) ← shade_hit\((\w+), (\w+)\)")]
    public void WhenShadeHit(string name, string world, string comp)
    {
        _context[name] = Get<World>(world).ShadeHit(Get<ComputationData>(comp), 5);
    }

    [When(@"(\w+) ← color_at\((\w+), (\w+)\)")]
    public void WhenColorAt(string name, string world, string ray)
    {
        _context[name] = Get<World>(world).ColorAt(Get<Ray>(ray));
    }

    [Then(@"(\w+) contains no objects")]
    public void ThenWorldContainsNoObjects(string name)
    {
        Assert.Equal(0, Get<World>(name).Objects.Count);
    }

    [Then(@"(\w+) has no light source")]
    public void ThenWorldHasNoLightSource(string name)
    {
        Assert.Equal(0, Get<World>(name).Lights.Count);
    }

    [Then(@"(\w+) contains (\w+)")]
    public void ThenWorldContainsObjects(string name, string obj)
    {
        Assert.Contains(Get<IShape>(obj), Get<World>(name).Objects);
    }

    [Then(@"(\w+)\.lights contains (\w+)")]
    public void ThenWorldContainsLight(string name, string obj)
    {
        Assert.Contains(Get<PointLight>(obj), Get<World>(name).Lights);
    }

    [Then(@"(\w+) = (\w+)\.material\.color")]
    public void ThenMaterialColorEquals(string name, string expected)
    {
        Assert.Equal(Get<IShape>(expected).Material.Color, Get<Tuple>(name));
    }
}
