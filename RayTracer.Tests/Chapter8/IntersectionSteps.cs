﻿using RayTracer.Math;
using RayTracer.Tracing;
using TechTalk.SpecFlow;
using Xunit;

namespace RayTracer.Tests.Chapter8;

public sealed class IntersectionSteps : CommonStepBase
{
    public IntersectionSteps(ScenarioContext context) : base(context) { }

    [Then(@"(\w+)\.over_point\.z < -EPSILON/2")]
    public void ThenOverPointZIsLess(string name)
    {
        Assert.True(Get<ComputationData>(name).OverPoint.Z < -Float.Epsilon / 2);
    }

    [Then(@"(\w+)\.point\.z > (\w+)\.over_point\.z")]
    public void ThenCompsPointZIsGreaterThanOverPointZ(string name1, string name2)
    {
        Assert.True(Get<ComputationData>(name1).Point.Z > Get<ComputationData>(name2).OverPoint.Z);
    }
}
