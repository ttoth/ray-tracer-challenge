﻿using RayTracer.Lights;
using RayTracer.Materials;
using RayTracer.Math;
using RayTracer.Shapes;
using TechTalk.SpecFlow;

namespace RayTracer.Tests.Chapter8;

public sealed class MaterialSteps : CommonStepBase
{
    public MaterialSteps(ScenarioContext context) : base(context) { }

    [When(@"(\w+) ← lighting\((\w+), (\w+), (\w+), (\w+), (\w+), (\w+)\)")]
    public void WhenLighting(string name, string m, string light, string position, string eye, string normal, string inShadow)
    {
        _context[name] = new Sphere { Material = Get<Material>(m) }.Lighting(Get<PointLight>(light), Get<Tuple>(position), Get<Tuple>(eye), Get<Tuple>(normal), Get<bool>(inShadow));
    }
}
