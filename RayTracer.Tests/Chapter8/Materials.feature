﻿Feature: Materials

Background:
  Given m ← material()
    And position ← point(0, 0, 0)

Scenario: Lighting with the surface in shadow
  Given eyev ← vector(0, 0, -1)
    And normalv ← vector(0, 0, -1)
    And light ← point_light(point(0, 0, -10), color(1, 1, 1))
    And in_shadow ← true
   When result ← lighting(m, light, position, eyev, normalv, in_shadow)
   Then result = color(0.1, 0.1, 0.1)
