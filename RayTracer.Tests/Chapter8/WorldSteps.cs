﻿using RayTracer.Math;
using RayTracer.Shapes;
using TechTalk.SpecFlow;
using Xunit;

namespace RayTracer.Tests.Chapter8;

public sealed class WorldSteps : CommonStepBase
{
    public WorldSteps(ScenarioContext context) : base(context) { }

    [Then(@"is_shadowed\((\w+), (\w+)\) is (false|true)")]
    public void ThenIsShadowedIs(string name, string p, bool expected)
    {
        var world = Get<World>(name);
        Assert.Equal(expected, world.IsShadowed(Get<Tuple>(p), world.Lights[0]));
    }

    [Given(@"(\w+) is added to (\w+)")]
    public void GivenObjectIsAddedToWorld(string name, string world)
    {
        Get<World>(world).Objects.Add(Get<IShape>(name));
    }
}
