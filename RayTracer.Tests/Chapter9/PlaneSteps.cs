﻿using RayTracer.Shapes;
using TechTalk.SpecFlow;

namespace RayTracer.Tests.Chapter9;

public sealed class PlaneSteps : CommonStepBase
{
    public PlaneSteps(ScenarioContext context) : base(context) { }

    [Given(@"(\w+) ← plane\(\)")]
    public void GivenPlane(string name)
    {
        _context[name] = new Plane();
    }
}
