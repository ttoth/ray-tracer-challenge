﻿using RayTracer.Math;
using RayTracer.Math.Matrices;
using RayTracer.Tests.Extensions;
using TechTalk.SpecFlow;
using Xunit;

namespace RayTracer.Tests.Chapter9;

public sealed class ShapeSteps : CommonStepBase
{
    public ShapeSteps(ScenarioContext context) : base(context) { }

    [Given(@"(\w+) ← test_shape\(\)")]
    public void GivenTestShape(string name)
    {
        _context[name] = new TestShape();
    }

    [Then(@"(\w+)\.transform = (\w+\(.*\))")]
    public void ThenTransformEquals(string name, Matrix44 expected)
    {
        Assert.Equal(expected, Get<object>(name).PropertyValue("transform"));
    }

    [Then(@"(\w+)\.saved_ray\.(\w+) = (.*)")]
    public void ThenSavedRayPropertyEquals(string name, string property, Tuple expected)
    {
        Assert.Equal(expected, Get<TestShape>(name).SavedRay.PropertyValue(property));
    }
}
