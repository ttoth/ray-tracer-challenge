﻿using RayTracer.Math;
using RayTracer.Shapes;
using RayTracer.Tracing;

namespace RayTracer.Tests.Chapter9;

internal sealed class TestShape : Shape
{
    public Ray SavedRay { get; private set; }

    public override Intersections LocalIntersect(Ray ray)
    {
        SavedRay = ray;
        return Intersections.Empty;
    }

    public override Tuple LocalNormalAt(Tuple point)
    {
        return Tuple.Vector(point.X, point.Y, point.Z);
    }
}
