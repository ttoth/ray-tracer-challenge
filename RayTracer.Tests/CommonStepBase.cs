﻿using RayTracer.Math.Matrices;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TechTalk.SpecFlow;

namespace RayTracer.Tests;

[Binding]
public abstract class CommonStepBase
{
    protected readonly ScenarioContext _context;

    protected CommonStepBase(ScenarioContext context)
    {
        _context = context;
        _context["IdentityMatrix"] = Matrix44.Identity;
        _context["identity_matrix"] = Matrix44.Identity;
        _context["infinity"] = double.PositiveInfinity;
        _context["-infinity"] = double.NegativeInfinity;
    }

    protected Matrix44 Matrix4x4(string name) => _context[name] switch
    {
        Matrix44 matrix44 => matrix44,
        Matrix matrix => new Matrix44(matrix),
        _ => throw new System.InvalidCastException($"{name} is not a Matrix")
    };

    protected T Get<T>(string name) => (T)_context[name];
    protected IEnumerable<object> GetEnumerable(string name) => Get<IEnumerable>(name).Cast<object>();
}
