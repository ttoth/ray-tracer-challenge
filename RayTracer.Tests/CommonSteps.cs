﻿using RayTracer.Tests.Extensions;
using System;
using System.Collections;
using TechTalk.SpecFlow;
using Xunit;

namespace RayTracer.Tests;

public sealed class CommonSteps : CommonStepBase
{
    public CommonSteps(ScenarioContext context) : base(context)
    {
        _context["true"] = true;
        _context["false"] = false;
    }

    [Given(@"(\w+) ← (-?[\d\.\/π√]+)")]
    public void GivenFloat(string name, double b)
    {
        _context[name] = b;
    }

    [Given(@"(\w+) ← (true|false)")]
    public void GivenBool(string name, bool b)
    {
        _context[name] = b;
    }


    [Given(@"(\w+)\.([a-zA-Z]\w*) ← (-?[\d\.]+)")]
    [When(@"(\w+)\.([a-zA-Z]\w*) ← (-?[\d\.]+)")]
    public void WhenPropertySet(string name, string property, double value)
    {
        var obj = Get<object>(name);
        obj.SetPropertyValue(property, value);
    }

    [Given(@"(\w+)\.([a-zA-Z]\w*) ← (true|false)")]
    [When(@"(\w+)\.([a-zA-Z]\w*) ← (true|false)")]
    public void WhenPropertySet(string name, string property, bool value)
    {
        var obj = Get<object>(name);
        obj.SetPropertyValue(property, value);
    }

    [Then(@"(\w+) = (\w+)")]
    public void ThenEquals(string a, string b)
    {
        Assert.Equal(_context[b], _context[a]);
    }

    [Then(@"(\w+) = (-?[\d\.\/π√]+)")]
    public void ThenFloatEquals(string name, double expected)
    {
        Assert.Equal(expected, Get<double>(name), 5);
    }

    [Then(@"(\w+) != (\w+)")]
    public void ThenNotEquals(string a, string b)
    {
        Assert.NotEqual(_context[b], _context[a]);
    }

    [Then(@"(\w+) is nothing")]
    public void ThenIsNothing(string name)
    {
        Assert.Null(Get<object>(name));
    }

    [Then(@"(\w+) is empty")]
    public void ThenEmpty(string name)
    {
        Assert.Empty(Get<IEnumerable>(name));
    }

    [Then(@"(\w+)\.([a-zA-Z]\w*) = (-?[\d\.\/π√]+)")]
    public void ThenPropertyEquals(string name, string property, double expected)
    {
        var obj = Get<object>(name);
        var prop = obj.PropertyValue(property) ?? throw new NullReferenceException(name);
        Assert.Equal(expected, (double)Convert.ChangeType(prop, typeof(double)), 5);
    }

    [Then(@"(\w+)\.([a-zA-Z]\w*)\.([a-zA-Z]\w*) = (-?[\d\.\/π√]+)")]
    public void ThenPropertyEquals(string name, string property1, string property2, double expected)
    {
        var obj = Get<object>(name);
        var prop = obj.PropertyValue(property1)?.PropertyValue(property2) ?? throw new NullReferenceException(name);
        Assert.Equal(expected, (double)Convert.ChangeType(prop, typeof(double)), 5);
    }

    [Then(@"(\w+)\.([a-zA-Z]\w*) = (-?[a-zA-Z]\w*)")]
    public void ThenPropertyEquals(string name, string property, string expected)
    {
        var obj = Get<object>(name);
        var prop = obj.PropertyValue(property);

        Assert.Equal(Get<object>(expected), prop);
    }

    [Then(@"(\w+)\.([a-zA-Z]\w*) = ((?>color|vector|point)\(.*\))")]
    public void ThenPropertyEquals(string name, string property, Math.Tuple expected)
    {
        var obj = Get<object>(name);
        var prop = obj.PropertyValue(property);

        Assert.Equal(expected, prop);
    }
}
