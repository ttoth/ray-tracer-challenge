﻿using RayTracer.Materials;
using RayTracer.Math;
using Xunit;

namespace RayTracer.Tests.EntitiesTests;

public class MaterialTests
{
    [Fact]
    public void SameMaterialsEquals()
    {
        var material1 = new Material();
        var material2 = new Material();

        Assert.Equal(material1, material2);
        Assert.Equal(material1.GetHashCode(), material2.GetHashCode());
        Assert.True(material1.Equals((object)material2));
    }

    [Theory]
    [InlineData(0, 2, 3, 4, 5)]
    [InlineData(1, 0, 3, 4, 5)]
    [InlineData(1, 2, 0, 4, 5)]
    [InlineData(1, 2, 3, 0, 5)]
    [InlineData(1, 2, 3, 4, 0)]
    public void DifferentMaterialsNotEquals(double r, double a, double d, double s, double sp)
    {
        var material1 = new Material { Color = Tuple.Color(1, 0, 0), Ambient = 2, Diffuse = 3, Shininess = 4, Specular = 5 };
        var material2 = new Material { Color = Tuple.Color(r, 0, 0), Ambient = a, Diffuse = d, Shininess = s, Specular = sp };

        Assert.NotEqual(material1, material2);
        Assert.NotEqual(material1.GetHashCode(), material2.GetHashCode());
    }
}
