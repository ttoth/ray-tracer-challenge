﻿using RayTracer.Math.Matrices;
using System;
using Xunit;

namespace RayTracer.Tests.EntitiesTests;

public class MatrixTests
{
    [Fact]
    public void MatrixEqualsItself()
    {
        var matrix = new Matrix22(1, 2, 3, 4);
        Assert.True(matrix.Equals(matrix));
    }

    [Fact]
    public void MatrixNotEqualsNull()
    {
        var matrix = new Matrix22(1, 2, 3, 4);
        Assert.False(matrix.Equals(null));
    }

    [Fact]
    public void MatrixEqualsSameMatrix()
    {
        var matrix1 = new Matrix22(1, 2, 3, 4);
        var matrix2 = new Matrix22(1, 2, 3, 4);

        Assert.Equal(matrix1.GetHashCode(), matrix2.GetHashCode());
        Assert.Equal(matrix1, matrix2);
    }

    [Fact]
    public void MatrixNotEqualsMatrixWithDifferentSize()
    {
        var matrix1 = new Matrix22(0, 0, 0, 0);
        var matrix2 = new Matrix33(0, 0, 0, 0, 0, 0, 0, 0, 0);

        Assert.NotEqual(matrix1.GetHashCode(), matrix2.GetHashCode());
        Assert.NotEqual<Matrix>(matrix1, matrix2);
    }

    [Fact]
    public void Matrix44EqualsMatrixWithSize4()
    {
        var matrix1 = new Matrix44(1, 2, 3, 4, 1, 2, 3, 4, 1, 2, 3, 4, 1, 2, 3, 4);
        var matrix2 = new Matrix44(matrix1);

        Assert.Equal(matrix1.GetHashCode(), matrix2.GetHashCode());
        Assert.Equal(matrix1, matrix2);
    }

    [Fact]
    public void CannotCreateMatrix44WithSizeDifferentThan4()
    {
        static void Act() => new Matrix44(new Matrix22(1, 2, 3, 4));

        Assert.Throws<ArgumentException>(Act);
    }

    [Fact]
    public void CannotInvertNonInvertibleMatrix()
    {
        var matrix = new Matrix22(0, 0, 0, 0);

        Assert.False(matrix.IsInvertible);
        Assert.Throws<MatrixNotInvertibleException>(matrix.Inverse);
    }

    [Fact]
    public void Matrix44CannotBeMultipliedByMatrixDifferentSize()
    {
        var matrix1 = new Matrix44(1, 2, 3, 4, 1, 2, 3, 4, 1, 2, 3, 4, 1, 2, 3, 4);
        var matrix2 = new Matrix22(1, 2, 3, 4);

        void Act() { var _ = matrix1 * matrix2; };

        Assert.Throws<ArgumentException>(Act);
    }
}
