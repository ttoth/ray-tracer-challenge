﻿using RayTracer.Lights;
using RayTracer.Math;
using Xunit;

namespace RayTracer.Tests.EntitiesTests;

public class PointLightTests
{
    [Fact]
    public void PointLightEqualsSamePointLight()
    {
        var pointLight1 = new PointLight(Tuple.Point(1, 2, 3), Tuple.Color(1, 2, 3));
        var pointLight2 = new PointLight(Tuple.Point(1, 2, 3), Tuple.Color(1, 2, 3));

        Assert.Equal(pointLight1.GetHashCode(), pointLight2.GetHashCode());
        Assert.Equal(pointLight1, pointLight2);
        Assert.True(pointLight1.Equals((object)pointLight2));
        Assert.True(pointLight1 == pointLight2);
    }

    [Fact]
    public void PointLightNotEqualsWhenOriginDifferent()
    {
        var pointLight1 = new PointLight(Tuple.Point(1, 2, 3), Tuple.Color(1, 2, 3));
        var pointLight2 = new PointLight(Tuple.Point(1, 2, 4), Tuple.Color(1, 2, 3));

        Assert.NotEqual(pointLight1.GetHashCode(), pointLight2.GetHashCode());
        Assert.NotEqual(pointLight1, pointLight2);
        Assert.True(pointLight1 != pointLight2);
    }

    [Fact]
    public void PointLightNotEqualsWhenIntensityDifferent()
    {
        var pointLight1 = new PointLight(Tuple.Point(1, 2, 3), Tuple.Color(1, 2, 3));
        var pointLight2 = new PointLight(Tuple.Point(1, 2, 3), Tuple.Color(1, 2, 4));

        Assert.NotEqual(pointLight1.GetHashCode(), pointLight2.GetHashCode());
        Assert.NotEqual(pointLight1, pointLight2);
        Assert.True(pointLight1 != pointLight2);
    }
}
