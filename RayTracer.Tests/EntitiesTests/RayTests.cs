﻿using RayTracer.Math;
using RayTracer.Tracing;
using Xunit;

namespace RayTracer.Tests.EntitiesTests;

public class RayTests
{
    [Fact]
    public void RayEqualsSameRay()
    {
        var ray1 = new Ray(Tuple.Point(1, 2, 3), Tuple.Vector(1, 2, 3));
        var ray2 = new Ray(Tuple.Point(1, 2, 3), Tuple.Vector(1, 2, 3));

        Assert.Equal(ray1.GetHashCode(), ray2.GetHashCode());
        Assert.Equal(ray1, ray2);
        Assert.True(ray1.Equals((object)ray2));
        Assert.True(ray1 == ray2);
    }

    [Fact]
    public void RayNotEqualsWhenOriginDifferent()
    {
        var ray1 = new Ray(Tuple.Point(1, 2, 3), Tuple.Vector(1, 2, 3));
        var ray2 = new Ray(Tuple.Point(1, 2, 4), Tuple.Vector(1, 2, 3));

        Assert.NotEqual(ray1.GetHashCode(), ray2.GetHashCode());
        Assert.NotEqual(ray1, ray2);
        Assert.True(ray1 != ray2);
    }

    [Fact]
    public void RayNotEqualsWhenDirectionDifferent()
    {
        var ray1 = new Ray(Tuple.Point(1, 2, 3), Tuple.Vector(1, 2, 3));
        var ray2 = new Ray(Tuple.Point(1, 2, 3), Tuple.Vector(1, 2, 4));

        Assert.NotEqual(ray1.GetHashCode(), ray2.GetHashCode());
        Assert.NotEqual(ray1, ray2);
        Assert.True(ray1 != ray2);
    }
}
