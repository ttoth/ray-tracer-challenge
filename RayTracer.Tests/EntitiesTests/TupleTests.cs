﻿using RayTracer.Math;
using Xunit;

namespace RayTracer.Tests.EntitiesTests;

public class TupleTests
{
    [Fact]
    public void TupleEqualsSameTuple()
    {
        var tuple1 = new Tuple(1, 2, 3, 4);
        var tuple2 = new Tuple(1, 2, 3, 4);

        Assert.Equal(tuple1.GetHashCode(), tuple2.GetHashCode());
        Assert.Equal(tuple1, tuple2);
        Assert.True(tuple1.Equals(tuple2));
        Assert.True(tuple1 == tuple2);
    }

    [Theory]
    [InlineData(2, 2, 3)]
    [InlineData(1, 3, 3)]
    [InlineData(1, 2, 4)]
    public void PointNotEqualsDifferentPoint(double x, double y, double z)
    {
        var point1 = Tuple.Point(1, 2, 3);
        var point2 = Tuple.Point(x, y, z);

        Assert.NotEqual(point1.GetHashCode(), point2.GetHashCode());
        Assert.NotEqual(point1, point2);
        Assert.True(point1 != point2);
    }

    [Fact]
    public void TupleNotEqualsDifferentObject()
    {
        var tuple = new Tuple(1, 2, 3, 4);
        var obj = tuple.GetHashCode();
        Assert.False(tuple.Equals(obj));
    }
}
