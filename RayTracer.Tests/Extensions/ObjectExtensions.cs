﻿using System;
using System.Linq;
using System.Reflection;

namespace RayTracer.Tests.Extensions;

internal static class ObjectExtensions
{
    public static object? PropertyValue(this object instance, string name)
    {
        return instance.Property(name).GetValue(instance);
    }

    public static void SetPropertyValue(this object instance, string name, object value)
    {
        instance.Property(name).SetValue(instance, value);
    }

    public static PropertyInfo Property(this object instance, string name)
    {
        name = name.Replace("_", "");
        var property = instance.GetType().GetProperties().SingleOrDefault(p => string.Equals(p.Name, name, StringComparison.OrdinalIgnoreCase))
            ?? throw new ArgumentException($"Could not find property '{name}' on type '{instance.GetType().Name}'", nameof(name));
        return property;
    }
}
