﻿using System.Collections.Generic;
using TechTalk.SpecFlow;

namespace RayTracer.Tests.Extensions;

public static class TableExtensions
{
    public static IEnumerable<string> AllCells(this Table table)
    {
        foreach (var cell in table.Header)
        {
            yield return cell;
        }

        foreach (var row in table.Rows)
        {
            foreach (var cell in row)
            {
                yield return cell.Value;
            }
        }
    }

    public static IEnumerable<KeyValuePair<string, string>> ToKeyValues(this Table table)
    {
        foreach (var cell in table.Rows[0])
        {
            yield return cell;
        }
    }
}
