﻿using System;
using System.Globalization;
using TechTalk.SpecFlow;
using static System.MathF;

namespace RayTracer.Tests.Transforms;

[Binding]
public sealed class FloatTransforms
{
    [StepArgumentTransformation(@"(-?(?>\d*π|√?\d+)(?>\/(?>\d+π|√?\d+))?|NaN)")]
    public double TransformFloat(string expression) => ParseFloat(expression);

    public static double[] ParseArguments(string expression) => Array.ConvertAll(expression.Split(','), ParseFloat);

    public static double ParseFloat(string expression)
    {
        expression = expression.Trim();
        var sign = expression[0] == '-' ? -1 : 1;
        expression = expression.TrimStart('-');
        var parts = expression.Split('/');

        if (parts.Length == 1)
        {
            expression = parts[0];

            if (expression == "NaN")
            {
                return double.NaN;
            }

            if (expression.EndsWith('π'))  //nπ
            {
                return sign * PI * (expression.Length == 1 ? 1 : int.Parse(expression.TrimEnd('π')));
            }

            if (expression[0] == '√')    // √n
            {
                return sign * Sqrt(int.Parse(expression.Substring(1)));
            }

            return sign * double.Parse(expression, NumberStyles.Float, CultureInfo.InvariantCulture);
        }

        return sign * ParseFloat(parts[0]) / ParseFloat(parts[1]);
    }
}
