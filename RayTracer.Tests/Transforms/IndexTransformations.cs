﻿using TechTalk.SpecFlow;

namespace RayTracer.Tests.Transforms;

[Binding]
public sealed class IndexTransformations
{
    [StepArgumentTransformation(@"(first|second|third|\d+th)")]
    public int TransformIndex(string expression) => expression switch
    {
        "first" => 0,
        "second" => 1,
        "third" => 2,
        _ => int.Parse(expression[0..-2])
    };
}
