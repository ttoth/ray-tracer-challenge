﻿using RayTracer.Math.Matrices;
using RayTracer.Tests.Extensions;
using System;
using System.Globalization;
using System.Linq;
using TechTalk.SpecFlow;

namespace RayTracer.Tests.Transforms;

[Binding]
public sealed class MatrixTransforms
{
    [StepArgumentTransformation("")]
    public Matrix44 TransformMatrix44(Table table)
    {
        if (table.RowCount != 3 || table.Header.Count != 4)
            throw new ArgumentException("Table size is not correct");

        var m = GetCells(table);
        return new Matrix44(m[0], m[1], m[2], m[3],
                            m[4], m[5], m[6], m[7],
                            m[8], m[9], m[10], m[11],
                            m[12], m[13], m[14], m[15]);
    }

    [StepArgumentTransformation(@"(translation|scaling|rotation_[xyz]|shearing)\((.*)\)")]
    public Matrix44 TransformMatrix(string function, string arguments) => TransformMatrixImpl(function, arguments);

    [StepArgumentTransformation]
    public Matrix33 TransformMatrix33(Table table)
    {
        if (table.RowCount != 2 || table.Header.Count != 3)
            throw new ArgumentException("Table size is not correct");

        var m = GetCells(table);
        return new Matrix33(m[0], m[1], m[2],
                            m[3], m[4], m[5],
                            m[6], m[7], m[8]);
    }

    [StepArgumentTransformation]
    public Matrix22 TransformMatrix22(Table table)
    {
        if (table.RowCount != 1 || table.Header.Count != 2)
            throw new ArgumentException("Table size is not correct");

        var m = GetCells(table);
        return new Matrix22(m[0], m[1],
                            m[2], m[3]);
    }

    public static Matrix44 ParseMatrix(string input)
    {
        var parts = input.Split('(', ')');
        return TransformMatrixImpl(parts[0], parts[1]);
    }

    private static Matrix44 TransformMatrixImpl(string function, string arguments)
    {
        var args = FloatTransforms.ParseArguments(arguments);

        return function switch
        {
            "translation" => Transformation.Translation(args[0], args[1], args[2]),
            "scaling" => Transformation.Scaling(args[0], args[1], args[2]),
            "shearing" => Transformation.Shearing(args[0], args[1], args[2], args[3], args[4], args[5]),
            "rotation_x" => Transformation.RotationX(args[0]),
            "rotation_y" => Transformation.RotationY(args[0]),
            "rotation_z" => Transformation.RotationZ(args[0]),
            _ => throw new ArgumentException($"{function} is invalid", nameof(function))
        };
    }

    private double[] GetCells(Table table) => table.AllCells().Select(c => double.Parse(c, NumberStyles.Float, CultureInfo.InvariantCulture)).ToArray();
}
