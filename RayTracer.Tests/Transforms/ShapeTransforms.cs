﻿using RayTracer.Shapes;
using RayTracer.Tests.Chapter10;
using RayTracer.Tests.Chapter11;
using RayTracer.Tests.Extensions;
using TechTalk.SpecFlow;

namespace RayTracer.Tests.Transforms;

[Binding]
public sealed class ShapeTransforms
{
    [StepArgumentTransformation(@"sphere\(\) with:")] public Sphere TransformSphere(Table table) => ApplyTable(new Sphere(), table);
    [StepArgumentTransformation(@"glass_sphere\(\) with:")] public GlassSphere TransformGlassSphere(Table table) => ApplyTable(new GlassSphere(), table);
    [StepArgumentTransformation(@"plane\(\) with:")] public Plane TransformPlane(Table table) => ApplyTable(new Plane(), table);

    public static TShape ApplyTable<TShape>(TShape shape, Table table) where TShape : IShape
    {
        var props = table.ToKeyValues();
        foreach (var prop in props)
        {
            var parts = prop.Key.Split('.');
            switch (parts[0])
            {
                case "material":
                    switch (parts[1])
                    {
                        case "color":
                            shape.Material.Color = TupleTransforms.ParseColor(prop.Value);
                            break;
                        case "pattern":
                            shape.Material.Pattern = new TestPattern();
                            break;
                        default:
                            shape.Material.SetPropertyValue(parts[1], FloatTransforms.ParseFloat(prop.Value));
                            break;
                    }
                    break;
                case "transform":
                    shape.Transform = MatrixTransforms.ParseMatrix(prop.Value);
                    break;
                default: throw new System.NotImplementedException(parts[0]);
            }
        }

        return shape;
    }
}
