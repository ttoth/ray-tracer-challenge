﻿using RayTracer.Math;
using TechTalk.SpecFlow;

namespace RayTracer.Tests.Transforms;

[Binding]
public sealed class TupleTransforms
{
    [StepArgumentTransformation(@"(vector|tuple|point|color)\((.*)\)")]
    public Tuple TransformTuple(string type, string arguments) => TransformTupleImpl(type, arguments);

    public static Tuple ParseColor(string input) => TransformTupleImpl("color", input.Trim('(', ')', ' '));

    private static Tuple TransformTupleImpl(string type, string arguments)
    {
        var args = FloatTransforms.ParseArguments(arguments);

        return type switch
        {
            "tuple" => new Tuple(args[0], args[1], args[2], args[3]),
            "vector" => Tuple.Vector(args[0], args[1], args[2]),
            "point" => Tuple.Point(args[0], args[1], args[2]),
            "color" => Tuple.Color(args[0], args[1], args[2]),
            _ => throw new System.ArgumentException($"{type} is invalid", nameof(type))
        };
    }
}
