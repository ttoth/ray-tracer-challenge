﻿using RayTracer.Math;
using RayTracer.Math.Matrices;
using RayTracer.Tracing;

namespace RayTracer;

public class Camera
{
    public Camera(int hsize, int vsize, double fieldOfView)
    {
        HSize = hsize;
        VSize = vsize;
        FieldOfView = fieldOfView;
        Transform = Matrix44.Identity;

        var halfView = System.Math.Tan(FieldOfView / 2);
        var aspect = HSize / (double)VSize;

        if (aspect >= 1)
        {
            HalfWidth = halfView;
            HalfHeight = halfView / aspect;
        }
        else
        {
            HalfWidth = halfView * aspect;
            HalfHeight = halfView;
        }

        PixelSize = HalfWidth * 2 / HSize;
    }

    public Matrix44 Transform { get; set; }
    public int HSize { get; }
    public int VSize { get; }
    public double FieldOfView { get; }
    public double PixelSize { get; }
    public double HalfWidth { get; }
    public double HalfHeight { get; }

    public Ray RayForPixel(int px, int py)
    {
        // the offset from the edge of the canvas to the pixel's center​ 
        var xOffset = (px + 0.5) * PixelSize;
        var yOffset = (py + 0.5) * PixelSize;

        // the untransformed coordinates of the pixel in world space.
        var worldX = HalfWidth - xOffset;
        var worldY = HalfHeight - yOffset;

        // using the camera matrix, transform the canvas point and the origin, and then compute the ray's direction vector
        var inverse = Transform.Inverse();
        var pixel = inverse * Tuple.Point(worldX, worldY, -1);
        var origin = inverse * Tuple.Point(0, 0, 0);
        var direction = (pixel - origin).Normalize();

        return new Ray(origin, direction);
    }

    public Canvas Render(World world)
    {
        var image = new Canvas(HSize, VSize);
        System.Threading.Tasks.Parallel.For(0, VSize, y =>
        //for (int y = 0; y < VSize; ++y)
        {
            for (int x = 0; x < HSize; ++x)
            {
                var ray = RayForPixel(x, y);
                var color = world.ColorAt(ray);
                image.WritePixel(x, y, color);
            }
        }
        );

        return image;
    }
}
