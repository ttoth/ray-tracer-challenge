﻿using RayTracer.Math;

namespace RayTracer;

public class Canvas
{
    private readonly Tuple[,] _canvas;

    public Canvas(int width, int height)
    {
        Width = width;
        Height = height;

        _canvas = new Tuple[width, height];
    }

    public int Width { get; }
    public int Height { get; }

    public void WritePixel(int x, int y, Tuple color)
    {
        _canvas[x, y] = color;
    }

    public Tuple PixelAt(int x, int y)
    {
        return _canvas[x, y];
    }
}
