﻿using System.Text;
using static System.Math;

namespace RayTracer;

public static class CanvasExtensions
{
    public static string ToPPM(this Canvas canvas)
    {
        var sb = new StringBuilder();
        sb.AppendLine("P3")
          .Append(canvas.Width).Append(' ').Append(canvas.Height).AppendLine()
          .AppendLine("255");

        for (int y = 0; y < canvas.Height; ++y)
        {
            for (int x = 0; x < canvas.Width; ++x)
            {
                if (x > 0) Separator(x, 0);

                var color = canvas.PixelAt(x, y);
                sb.Append(PixelValue(color.Red)); Separator(x, 1)
                  .Append(PixelValue(color.Green)); Separator(x, 2)
                  .Append(PixelValue(color.Blue));
            }

            sb.AppendLine();
        }

        return sb.ToString();

        StringBuilder Separator(int x, int pixel) => (x * 3 + pixel) % 17 == 0 ? sb.AppendLine() : sb.Append(' ');

        static int PixelValue(double value) => Clamp((int)Round(value * 255), 0, 255);
    }
}
