﻿using RayTracer.Math;

namespace RayTracer.Lights;

public readonly record struct PointLight(Tuple Position, Tuple Intensity);
