﻿using RayTracer.Math;
using RayTracer.Patterns;

namespace RayTracer.Materials;

public record class Material
{
    public Tuple Color
    {
        get => Pattern is SolidPattern sp ? sp.Color : Tuple.Color(1, 1, 1);
        set => Pattern = new SolidPattern(value);
    }

    public double Ambient { get; init; } = 0.1;
    public double Diffuse { get; init; } = 0.9;
    public double Specular { get; init; } = 0.9;
    public double Shininess { get; init; } = 200;
    public double Reflective { get; init; }
    public double Transparency { get; init; }
    public double RefractiveIndex { get; init; } = 1;
    public IPattern Pattern { get; set; } = SolidPattern.White;
}
