﻿namespace RayTracer.Math;

public static class Float
{
    public const double Epsilon = 0.00003;
    public static bool Equals(double a, double b) => System.Math.Abs(a - b) < Epsilon;
    public static double Max(double a, double b, double c) => System.Math.Max(a, System.Math.Max(b, c));
    public static double Min(double a, double b, double c) => System.Math.Min(a, System.Math.Min(b, c));
}
