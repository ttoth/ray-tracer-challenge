﻿using System;
using System.Diagnostics;

namespace RayTracer.Math.Matrices;

[DebuggerDisplay("M({Size})")]
public class Matrix : IEquatable<Matrix>
{
    internal readonly double[,] _matrix;
    private readonly Lazy<double> _determinant;
    private readonly Lazy<Matrix> _inverse;

    protected Matrix(double[,] matrix)
    {
        _matrix = matrix;
        _determinant = new Lazy<double>(CalculateDeterminant);
        _inverse = new Lazy<Matrix>(CalculateInverse);
    }

    public double this[int index1, int index2] => _matrix[index1, index2];

    public int Size => _matrix.GetLength(0);
    public double Determinant => _determinant.Value;
    public bool IsInvertible => Determinant != 0;

    public Matrix Transpose()
    {
        var result = new double[Size, Size];
        for (var row = 0; row < Size; ++row)
            for (var col = 0; col < Size; ++col)
                result[row, col] = _matrix[col, row];

        return new Matrix(result);
    }

    public Matrix SubMatrix(int row, int column)
    {
        var result = new double[Size - 1, Size - 1];
        var offsetX = 0;

        for (var x = 0; x < Size; ++x)
        {
            if (x == row)
            {
                offsetX = 1;
                continue;
            }

            var offsetY = 0;

            for (var y = 0; y < Size; ++y)
            {
                if (y == column)
                {
                    offsetY = 1;
                    continue;
                }

                result[x - offsetX, y - offsetY] = _matrix[x, y];
            }
        }

        return new Matrix(result);
    }

    public Matrix Inverse() => _inverse.Value;
    public double Minor(int row, int column) => SubMatrix(row, column).Determinant;
    public double Cofactor(int row, int column) => Minor(row, column) * ((row + column) % 2 * -2 + 1);

    public bool Equals(Matrix? other)
    {
        if (other is null) return false;
        if (ReferenceEquals(other, this)) return true;
        if (ReferenceEquals(other._matrix, _matrix)) return true;

        if (other.Size != Size)
        {
            return false;
        }

        for (int x = 0; x < Size; ++x)
            for (int y = 0; y < Size; ++y)
                if (!Float.Equals(other._matrix[x, y], _matrix[x, y]))
                    return false;

        return true;
    }

    public override bool Equals(object? obj) => Equals(obj as Matrix);

    public override int GetHashCode()
    {
        var hashCode = new HashCode();
        foreach (var m in _matrix) hashCode.Add(m);
        return hashCode.ToHashCode();
    }

    private double CalculateDeterminant()
    {
        if (Size == 2) return _matrix[0, 0] * _matrix[1, 1] - _matrix[0, 1] * _matrix[1, 0];

        double det = 0;
        for (int column = 0; column < Size; ++column)
            det += _matrix[0, column] * Cofactor(0, column);

        return det;
    }

    private Matrix CalculateInverse()
    {
        if (!IsInvertible) throw new MatrixNotInvertibleException();

        var inverse = new double[Size, Size];
        for (var row = 0; row < Size; ++row)
            for (var col = 0; col < Size; ++col)
                inverse[col, row] = Cofactor(row, col) / Determinant;

        return new Matrix(inverse);
    }
}
