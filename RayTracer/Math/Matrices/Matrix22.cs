﻿namespace RayTracer.Math.Matrices;

public class Matrix22 : Matrix
{
    public Matrix22(double m11, double m12,
                    double m21, double m22)
        : base(new double[2, 2] {
            { m11, m12 },
            { m21, m22 }})
    {
    }
}
