﻿namespace RayTracer.Math.Matrices;

public class Matrix33 : Matrix
{
    public Matrix33(double m11, double m12, double m13,
                    double m21, double m22, double m23,
                    double m31, double m32, double m33)
        : base(new double[3, 3] {
            { m11, m12, m13 },
            { m21, m22, m23 },
            { m31, m32, m33 }})
    {
    }
}
