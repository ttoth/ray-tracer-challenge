﻿using System;

namespace RayTracer.Math.Matrices;

public class Matrix44 : Matrix
{
    public Matrix44(double m11, double m12, double m13, double m14,
                    double m21, double m22, double m23, double m24,
                    double m31, double m32, double m33, double m34,
                    double m41, double m42, double m43, double m44)
        : base(new double[4, 4] {
            { m11, m12, m13, m14 },
            { m21, m22, m23, m24 },
            { m31, m32, m33, m34 },
            { m41, m42, m43, m44 }})
    {
    }

    public Matrix44(Matrix matrix) : base(matrix._matrix)
    {
        if (Size != 4) throw new ArgumentException("Incorrect size.", nameof(matrix));
    }

    private Matrix44(double[,] matrix) : base(matrix) { }

    public new Matrix44 Transpose() => new(base.Transpose());
    public new Matrix44 Inverse() => new(base.Inverse());

    public static Matrix44 operator *(Matrix44 a, Matrix b)
    {
        if (b.Size != 4) throw new ArgumentException("Size has to be 4", nameof(b));

        var result = new double[4, 4];
        for (var row = 0; row < 4; ++row)
            for (var col = 0; col < 4; ++col)
                result[row, col] = a[row, 0] * b[0, col] +
                                   a[row, 1] * b[1, col] +
                                   a[row, 2] * b[2, col] +
                                   a[row, 3] * b[3, col];

        return new Matrix44(result);
    }

    public static Tuple operator *(Matrix44 a, Tuple b)
    {
        Span<double> t = stackalloc double[4];
        for (var row = 0; row < 4; ++row)
            t[row] = a[row, 0] * b.X +
                     a[row, 1] * b.Y +
                     a[row, 2] * b.Z +
                     a[row, 3] * b.W;

        return new Tuple(t[0], t[1], t[2], t[3]);
    }

    public static Matrix44 Identity { get; } = new Matrix44(new double[4, 4]
    {
        { 1, 0, 0, 0},
        { 0, 1, 0, 0},
        { 0, 0, 1, 0},
        { 0, 0, 0, 1}
    });
}
