﻿using System;
using System.Runtime.Serialization;

namespace RayTracer.Math.Matrices;

public class MatrixNotInvertibleException : Exception
{
    public MatrixNotInvertibleException() : this("Matrix is not invertible.") { }
    public MatrixNotInvertibleException(string message) : base(message) { }
    public MatrixNotInvertibleException(string message, Exception innerException) : base(message, innerException) { }
    protected MatrixNotInvertibleException(SerializationInfo info, StreamingContext context) : base(info, context) { }
}
