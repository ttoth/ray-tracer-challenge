﻿using static System.Math;

namespace RayTracer.Math.Matrices;

public static class Transformation
{
    public static Matrix44 Translation(double x, double y, double z) => new(
        1, 0, 0, x,
        0, 1, 0, y,
        0, 0, 1, z,
        0, 0, 0, 1);

    public static Matrix44 Scaling(double x, double y, double z) => new(
        x, 0, 0, 0,
        0, y, 0, 0,
        0, 0, z, 0,
        0, 0, 0, 1);

    public static Matrix44 RotationX(double r)
    {
        double s = Sin(r), c = Cos(r);
        return new Matrix44(
            1, 0, 0, 0,
            0, c, -s, 0,
            0, s, c, 0,
            0, 0, 0, 1);
    }

    public static Matrix44 RotationY(double r)
    {
        double s = Sin(r), c = Cos(r);
        return new Matrix44(
            c, 0, s, 0,
            0, 1, 0, 0,
           -s, 0, c, 0,
            0, 0, 0, 1);
    }

    public static Matrix44 RotationZ(double r)
    {
        double s = Sin(r), c = Cos(r);
        return new Matrix44(
            c, -s, 0, 0,
            s, c, 0, 0,
            0, 0, 1, 0,
            0, 0, 0, 1);
    }

    public static Matrix44 Shearing(double xy, double xz, double yx, double yz, double zx, double zy) => new(
        1, xy, xz, 0,
        yx, 1, yz, 0,
        zx, zy, 1, 0,
          0, 0, 0, 1);

    public static Matrix44 ViewTransform(Tuple from, Tuple to, Tuple up)
    {
        var forward = (to - from).Normalize();
        var upn = up.Normalize();
        var left = forward.Cross(upn);
        var trueUp = left.Cross(forward);

        var orientation = new Matrix44(
            left.X, left.Y, left.Z, 0,
            trueUp.X, trueUp.Y, trueUp.Z, 0,
            -forward.X, -forward.Y, -forward.Z, 0,
            0, 0, 0, 1);

        return orientation * Translation(-from.X, -from.Y, -from.Z);
    }
}
