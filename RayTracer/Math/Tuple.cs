﻿using System.Diagnostics;

namespace RayTracer.Math;

[DebuggerDisplay("({X},{Y},{Z},{W})")]
public readonly record struct Tuple(double X, double Y, double Z, double W)
{
    public double Red => X;
    public double Green => Y;
    public double Blue => Z;

    public static Tuple Point(double x, double y, double z) => new(x, y, z, 1);
    public static Tuple Vector(double x, double y, double z) => new(x, y, z, 0);
    public static Tuple Color(double red, double green, double blue) => new(red, green, blue, 0);

    public bool Equals(Tuple other) => Float.Equals(X, other.X) && Float.Equals(Y, other.Y) && Float.Equals(Z, other.Z) && Float.Equals(W, other.W);

    public static Tuple operator -(Tuple a) => new(-a.X, -a.Y, -a.Z, -a.W);
    public static Tuple operator +(Tuple a, Tuple b) => new(a.X + b.X, a.Y + b.Y, a.Z + b.Z, a.W + b.W);
    public static Tuple operator -(Tuple a, Tuple b) => new(a.X - b.X, a.Y - b.Y, a.Z - b.Z, a.W - b.W);
    public static Tuple operator *(Tuple a, Tuple b) => new(a.X * b.X, a.Y * b.Y, a.Z * b.Z, a.W * b.W);

    public static Tuple operator *(Tuple tuple, double x) => new(tuple.X * x, tuple.Y * x, tuple.Z * x, tuple.W * x);
    public static Tuple operator /(Tuple tuple, double x) => new(tuple.X / x, tuple.Y / x, tuple.Z / x, tuple.W / x);
}
