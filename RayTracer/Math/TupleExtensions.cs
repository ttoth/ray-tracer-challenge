﻿namespace RayTracer.Math;

public static class TupleExtensions
{
    public static bool IsPoint(this Tuple tuple) => tuple.W == 1;
    public static bool IsVector(this Tuple tuple) => tuple.W == 0;
    public static double Magnitude(this Tuple tuple) => System.Math.Sqrt(tuple.X * tuple.X + tuple.Y * tuple.Y + tuple.Z * tuple.Z + tuple.W * tuple.W);
    public static double Dot(this Tuple a, Tuple b) => a.X * b.X + a.Y * b.Y + a.Z * b.Z + a.W * b.W;

    public static Tuple Normalize(this Tuple tuple)
    {
        var magnitude = tuple.Magnitude();
        return new Tuple(tuple.X / magnitude, tuple.Y / magnitude, tuple.Z / magnitude, tuple.W / magnitude);
    }

    public static Tuple Cross(this Tuple a, Tuple b)
    {
        return Tuple.Vector(a.Y * b.Z - a.Z * b.Y, a.Z * b.X - a.X * b.Z, a.X * b.Y - a.Y * b.X);
    }

    public static Tuple Reflect(this Tuple vector, Tuple normal) => vector - normal * 2 * Dot(vector, normal);
}
