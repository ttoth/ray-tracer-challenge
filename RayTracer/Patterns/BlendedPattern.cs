﻿using RayTracer.Math;

namespace RayTracer.Patterns;

public class BlendedPattern : Pattern
{
    public BlendedPattern(Pattern a, Pattern b)
    {
        A = a;
        B = b;
    }

    public Pattern A { get; }
    public Pattern B { get; }

    public override Tuple PatternAt(Tuple point) => (A.PatternAt(point) + B.PatternAt(point)) / 2;
}
