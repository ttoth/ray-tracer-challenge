﻿using RayTracer.Math;
using static System.Math;

namespace RayTracer.Patterns;

public class CheckersPattern : Pattern
{
    public CheckersPattern(Tuple a, Tuple b)
    {
        A = a;
        B = b;
    }

    public Tuple A { get; }
    public Tuple B { get; }

    public override Tuple PatternAt(Tuple point) => (int)(Floor(point.X) + Floor(point.Y) + Floor(point.Z)) % 2 == 0 ? A : B;
}
