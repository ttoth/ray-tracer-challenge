﻿using RayTracer.Math;

namespace RayTracer.Patterns;

public class GradientPattern : Pattern
{
    public GradientPattern(Tuple a, Tuple b)
    {
        A = a;
        B = b;
    }

    public Tuple A { get; }
    public Tuple B { get; }

    public override Tuple PatternAt(Tuple point)
    {
        var distance = B - A;
        var fraction = point.X - System.Math.Floor(point.X);
        return A + distance * fraction;
    }
}
