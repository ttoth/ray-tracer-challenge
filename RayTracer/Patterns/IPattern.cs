﻿using RayTracer.Math;
using RayTracer.Math.Matrices;
using RayTracer.Shapes;

namespace RayTracer.Patterns;

public interface IPattern
{
    Matrix44 Transform { get; }
    Tuple PatternAtShape(IShape shape, Tuple point);
}
