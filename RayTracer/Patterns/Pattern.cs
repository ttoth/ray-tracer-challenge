﻿using RayTracer.Math;
using RayTracer.Math.Matrices;
using RayTracer.Shapes;

namespace RayTracer.Patterns;

public abstract class Pattern : IPattern
{
    public Matrix44 Transform { get; set; } = Matrix44.Identity;

    public Tuple PatternAtShape(IShape shape, Tuple point)
    {
        var objectPoint = shape.Transform.Inverse() * point;
        var patternPoint = Transform.Inverse() * objectPoint;
        return PatternAt(patternPoint);
    }

    public abstract Tuple PatternAt(Tuple point);
}
