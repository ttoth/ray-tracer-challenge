﻿using RayTracer.Math;
using static System.Math;

namespace RayTracer.Patterns;

public class RadialGradientPattern : Pattern
{
    public RadialGradientPattern(Tuple a, Tuple b)
    {
        A = a;
        B = b;
    }

    public Tuple A { get; }
    public Tuple B { get; }

    public override Tuple PatternAt(Tuple point)
    {
        var distance = B - A;
        var radius = Sqrt(point.X * point.X + point.Z * point.Z);
        var fraction = radius - Floor(radius);
        return A + distance * fraction;
    }
}
