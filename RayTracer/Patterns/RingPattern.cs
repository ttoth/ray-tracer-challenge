﻿using RayTracer.Math;
using static System.Math;

namespace RayTracer.Patterns;

public class RingPattern : Pattern
{
    public RingPattern(Tuple a, Tuple b)
    {
        A = a;
        B = b;
    }

    public Tuple A { get; }
    public Tuple B { get; }

    public override Tuple PatternAt(Tuple point) => (int)Floor(Sqrt(point.X * point.X + point.Z * point.Z)) % 2 == 0 ? A : B;
}
