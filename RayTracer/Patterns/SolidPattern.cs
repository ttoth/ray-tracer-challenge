﻿using RayTracer.Math;

namespace RayTracer.Patterns;

public class SolidPattern : Pattern, System.IEquatable<SolidPattern?>
{
    public static SolidPattern White { get; } = new SolidPattern(Tuple.Color(1, 1, 1));

    public SolidPattern(Tuple color)
    {
        Color = color;
    }

    public Tuple Color { get; }
    public override Tuple PatternAt(Tuple point) => Color;

    public override bool Equals(object? obj) => Equals(obj as SolidPattern);
    public bool Equals(SolidPattern? other) => other != null && Color == other.Color;
    public override int GetHashCode() => Color.GetHashCode();
}
