﻿using RayTracer.Math;

namespace RayTracer.Patterns;

public class StripePattern : Pattern
{
    public StripePattern(Tuple a, Tuple b)
    {
        A = a;
        B = b;
    }

    public Tuple A { get; }
    public Tuple B { get; }

    public override Tuple PatternAt(Tuple point) => (int)System.Math.Floor(point.X) % 2 == 0 ? A : B;
}
