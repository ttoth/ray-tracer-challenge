﻿using RayTracer.Math;
using RayTracer.Tracing;
using System.Collections.Generic;

namespace RayTracer.Shapes;

public abstract class CappedShape : Shape
{
    public double Maximum { get; set; } = double.PositiveInfinity;
    public double Minimum { get; set; } = double.NegativeInfinity;
    public bool Closed { get; set; }

    protected void IntersectCaps(Ray ray, List<Intersection> xs)
    {
        // caps only matter if the cylinder is closed, and might possibly be​ ​intersected by the ray.​
        if (!Closed || Float.Equals(ray.Direction.Y, 0))
        {
            return;
        }

        // check for an intersection with the lower end cap by intersecting​ ​the ray with the plane at y=cyl.minimum
        var t = (Minimum - ray.Origin.Y) / ray.Direction.Y;
        if (CheckCap(ray, t))
        {
            xs.Add(new Intersection(t, this));
        }

        // check for an intersection with the upper end cap by intersecting​ ​the ray with the plane at y=cyl.maximum
        t = (Maximum - ray.Origin.Y) / ray.Direction.Y;
        if (CheckCap(ray, t))
        {
            xs.Add(new Intersection(t, this));
        }
    }

    protected abstract bool CheckCap(Ray ray, double t);
}
