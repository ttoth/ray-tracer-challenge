﻿using RayTracer.Math;
using RayTracer.Tracing;
using static System.Math;

namespace RayTracer.Shapes;

public class Cube : Shape
{
    public override Intersections LocalIntersect(Ray ray)
    {
        var (xtmin, xtmax) = CheckAxis(ray.Origin.X, ray.Direction.X);
        var (ytmin, ytmax) = CheckAxis(ray.Origin.Y, ray.Direction.Y);
        var (ztmin, ztmax) = CheckAxis(ray.Origin.Z, ray.Direction.Z);

        var tmin = Float.Max(xtmin, ytmin, ztmin);
        var tmax = Float.Min(xtmax, ytmax, ztmax);

        return tmin > tmax
            ? Intersections.Empty
            : new Intersections(new Intersection(tmin, this), new Intersection(tmax, this));
    }

    public override Tuple LocalNormalAt(Tuple point)
    {
        var maxc = Float.Max(Abs(point.X), Abs(point.Y), Abs(point.Z));

        return maxc == Abs(point.X) ? Tuple.Vector(point.X, 0, 0) :
               maxc == Abs(point.Y) ? Tuple.Vector(0, point.Y, 0)
                                    : Tuple.Vector(0, 0, point.Z);
    }

    private (double min, double max) CheckAxis(double origin, double direction)
    {
        var tminNumerator = -1 - origin;
        var tmaxNumerator = +1 - origin;

        var tmin = tminNumerator / direction;
        var tmax = tmaxNumerator / direction;

        return tmin > tmax
            ? (tmax, tmin)
            : (tmin, tmax);
    }
}
