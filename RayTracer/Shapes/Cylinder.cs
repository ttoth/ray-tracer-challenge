﻿using RayTracer.Math;
using RayTracer.Tracing;
using System.Collections.Generic;
using static System.Math;

namespace RayTracer.Shapes;

public class Cylinder : CappedShape
{
    public override Intersections LocalIntersect(Ray ray)
    {
        var xs = new List<Intersection>();

        var a = ray.Direction.X * ray.Direction.X + ray.Direction.Z * ray.Direction.Z;
        if (a < Float.Epsilon)
        {
            IntersectCaps(ray, xs);
            return new Intersections(xs);
        }

        var b = 2 * (ray.Origin.X * ray.Direction.X + ray.Origin.Z * ray.Direction.Z);
        var c = ray.Origin.X * ray.Origin.X + ray.Origin.Z * ray.Origin.Z - 1;
        var disc = b * b - 4 * a * c;

        // ray does not intersect the cylinder
        if (disc < 0)
        {
            return Intersections.Empty;
        }

        var t0 = (-b - Sqrt(disc)) / (2 * a);
        var t1 = (-b + Sqrt(disc)) / (2 * a);

        var y0 = ray.Origin.Y + t0 * ray.Direction.Y;
        if (Minimum < y0 && y0 < Maximum)
        {
            xs.Add(new Intersection(t0, this));
        }

        var y1 = ray.Origin.Y + t1 * ray.Direction.Y;
        if (Minimum < y1 && y1 < Maximum)
        {
            xs.Add(new Intersection(t1, this));
        }

        IntersectCaps(ray, xs);
        return new Intersections(xs);
    }

    public override Tuple LocalNormalAt(Tuple point)
    {
        var dist = point.X * point.X + point.Z * point.Z;

        if (dist < 1 && point.Y >= Maximum - Float.Epsilon)
        {
            return Tuple.Vector(0, 1, 0);
        }

        if (dist < 1 && point.Y <= Minimum + Float.Epsilon)
        {
            return Tuple.Vector(0, -1, 0);
        }

        return Tuple.Vector(point.X, 0, point.Z);
    }

    protected override bool CheckCap(Ray ray, double t)
    {
        var hit = ray.Origin + ray.Direction * t;
        return (hit.X * hit.X + hit.Z * hit.Z) <= 1;
    }
}
