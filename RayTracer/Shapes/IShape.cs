﻿using RayTracer.Materials;
using RayTracer.Math;
using RayTracer.Math.Matrices;
using RayTracer.Tracing;

namespace RayTracer.Shapes;

public interface IShape
{
    Matrix44 Transform { get; set; }
    Material Material { get; set; }

    Tuple NormalAt(Tuple point);
    Intersections Intersect(Ray ray);
}
