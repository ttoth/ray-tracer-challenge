﻿using RayTracer.Math;
using RayTracer.Tracing;

namespace RayTracer.Shapes;

public class Plane : Shape
{
    public override Intersections LocalIntersect(Ray ray)
    {
        if (System.Math.Abs(ray.Direction.Y) < Float.Epsilon)
        {
            return Intersections.Empty;
        }

        var t = -ray.Origin.Y / ray.Direction.Y;
        return new Intersections(new Intersection(t, this));
    }

    public override Tuple LocalNormalAt(Tuple point)
    {
        return Tuple.Vector(0, 1, 0);
    }
}
