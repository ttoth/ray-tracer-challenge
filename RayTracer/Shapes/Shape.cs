﻿using RayTracer.Materials;
using RayTracer.Math;
using RayTracer.Math.Matrices;
using RayTracer.Tracing;

namespace RayTracer.Shapes;

public abstract class Shape : IShape, System.IEquatable<Shape>
{
    public Matrix44 Transform { get; set; } = Matrix44.Identity;
    public Material Material { get; set; } = new Material();

    public Intersections Intersect(Ray ray)
    {
        var localRay = ray.Transform(Transform.Inverse());
        return LocalIntersect(localRay);
    }

    public Tuple NormalAt(Tuple point)
    {
        var inverseTransform = Transform.Inverse();
        var localPoint = inverseTransform * point;
        var localNormal = LocalNormalAt(localPoint);
        var worldNormal = (inverseTransform.Transpose() * localNormal) with { W = 0 };
        return worldNormal.Normalize();
    }

    public abstract Intersections LocalIntersect(Ray ray);
    public abstract Tuple LocalNormalAt(Tuple point);

    public override bool Equals(object? obj) => Equals(obj as Shape);
    public override int GetHashCode() => System.HashCode.Combine(Transform, Material);

    public bool Equals(Shape? other) =>
        other != null &&
        GetType() == other.GetType() &&
        Equals(Transform, other.Transform) &&
        Equals(Material, other.Material);
}
