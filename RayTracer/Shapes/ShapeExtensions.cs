﻿using RayTracer.Lights;
using RayTracer.Math;

namespace RayTracer.Shapes;

public static class ShapeExtensions
{
    public static Tuple Lighting(this IShape shape, PointLight light, Tuple point, Tuple eyev, Tuple normalv, bool inShadow)
    {
        var material = shape.Material;
        var color = material.Pattern.PatternAtShape(shape, point);

        // combine the surface color with the light's color/intensity​
        var effectiveColor = color * light.Intensity;

        // compute the ambient contribution
        var ambient = effectiveColor * material.Ambient;
        if (inShadow)
        {
            return ambient;
        }

        // find the direction to the light source
        var lightv = (light.Position - point).Normalize();

        // represents the cosine of the angle between the light vector and the normal vector.
        // negative number means the light is on the other side of the surface.
        var lightDotNormal = lightv.Dot(normalv);
        if (lightDotNormal < 0)
        {
            return ambient;
        }

        // compute the diffues contribution
        var diffuse = effectiveColor * material.Diffuse * lightDotNormal;

        // represents the cosine of the angle between the reflection vector and the eye vector.
        // negative number means the light reflects away from the eye.
        var reflectv = -lightv.Reflect(normalv);
        var reflectDotEye = reflectv.Dot(eyev);

        var specular = Tuple.Color(0, 0, 0);
        if (reflectDotEye > 0)
        {
            // compute the specular contribution
            var factor = System.Math.Pow(reflectDotEye, material.Shininess);
            specular = light.Intensity * material.Specular * factor;
        }

        // add the three contribution together to get the final shading
        return ambient + diffuse + specular;
    }
}
