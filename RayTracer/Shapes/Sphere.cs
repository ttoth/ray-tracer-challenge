﻿using RayTracer.Math;
using RayTracer.Tracing;
using static System.Math;

namespace RayTracer.Shapes;

public class Sphere : Shape
{
    public override Intersections LocalIntersect(Ray ray)
    {
        // remember: the sphere is centered at the world origin
        var sphere2ray = ray.Origin - Tuple.Point(0, 0, 0);
        var a = ray.Direction.Dot(ray.Direction);
        var b = 2 * ray.Direction.Dot(sphere2ray);
        var c = sphere2ray.Dot(sphere2ray) - 1;

        var discriminant = b * b - 4 * a * c;
        if (discriminant < 0)
        {
            return Intersections.Empty;
        }

        return new Intersections
        (
            new Intersection((-b - Sqrt(discriminant)) / (2 * a), this),
            new Intersection((-b + Sqrt(discriminant)) / (2 * a), this)
        );
    }

    public override Tuple LocalNormalAt(Tuple point)
    {
        return point - Tuple.Point(0, 0, 0);
    }
}
