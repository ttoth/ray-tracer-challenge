﻿using RayTracer.Math;
using RayTracer.Shapes;

namespace RayTracer.Tracing;

public class ComputationData
{
    public ComputationData(IShape obj)
    {
        Object = obj;
    }

    public double T { get; set; }
    public double N1 { get; set; }
    public double N2 { get; set; }
    public IShape Object { get; set; }
    public Tuple Point { get; set; }
    public Tuple UnderPoint { get; set; }
    public Tuple OverPoint { get; set; }
    public Tuple EyeV { get; set; }
    public Tuple NormalV { get; set; }
    public Tuple ReflectV { get; set; }
    public bool Inside { get; set; }
}
