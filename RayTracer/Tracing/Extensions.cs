﻿using RayTracer.Math;

namespace RayTracer.Tracing;

public static class Extensions
{
    public static double Schlick(this ComputationData comps)
    {
        // find the cosine of the angle between the eye and normal vectors
        var cos = comps.EyeV.Dot(comps.NormalV);

        // total internal reflection can only occur if n1 > n2
        if (comps.N1 > comps.N2)
        {
            var n = comps.N1 / comps.N2;
            var sin2t = n * n * (1 - cos * cos);
            if (sin2t > 1)
                return 1;

            // compute cosine of theta_t using trig identity
            // when n1 > n2, use cos(theta_t) instead
            cos = System.Math.Sqrt(1 - sin2t);
        }

        var r = (comps.N1 - comps.N2) / (comps.N1 + comps.N2);
        var r0 = r * r;
        return r0 + (1 - r0) * System.Math.Pow(1 - cos, 5);
    }
}
