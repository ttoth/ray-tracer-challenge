﻿using RayTracer.Math;
using RayTracer.Shapes;
using System.Collections.Generic;
using System.Linq;

namespace RayTracer.Tracing;

public class Intersection
{
    public Intersection(double t, IShape obj)
    {
        T = t;
        Object = obj;
    }

    public double T { get; }
    public IShape Object { get; }

    public ComputationData PrepareComputations(Ray ray, Intersections intersections)
    {
        var point = ray.Position(T);
        var comps = new ComputationData(Object)
        {
            T = T,
            Point = point,
            EyeV = -ray.Direction,
            NormalV = Object.NormalAt(point)
        };

        if (comps.NormalV.Dot(comps.EyeV) < 0)
        {
            comps.Inside = true;
            comps.NormalV = -comps.NormalV;
        }

        comps.UnderPoint = comps.Point - comps.NormalV * Float.Epsilon;
        comps.OverPoint = comps.Point + comps.NormalV * Float.Epsilon;
        comps.ReflectV = ray.Direction.Reflect(comps.NormalV);

        var containers = new List<IShape>();

        foreach (var i in intersections)
        {
            if (i == this)
                comps.N1 = containers.LastOrDefault()?.Material.RefractiveIndex ?? 1;

            if (!containers.Remove(i.Object))
                containers.Add(i.Object);

            if (i == this)
            {
                comps.N2 = containers.LastOrDefault()?.Material.RefractiveIndex ?? 1;
                break;
            }
        }

        return comps;
    }
}
