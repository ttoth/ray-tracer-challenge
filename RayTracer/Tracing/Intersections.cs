﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace RayTracer.Tracing;

public class Intersections : IReadOnlyList<Intersection>
{
    public static readonly Intersections Empty = new();

    private readonly IReadOnlyList<Intersection> _intersections;
    private Intersections() => _intersections = Array.Empty<Intersection>();

    public Intersections(IReadOnlyList<Intersection> intersections) => _intersections = intersections;
    public Intersections(params Intersection[] intersections) => _intersections = intersections;

    public Intersection? Hit() => _intersections
        .Where(i => i.T >= 0)
        .OrderBy(i => i.T)
        .FirstOrDefault();

    public Intersection this[int index] => _intersections[index];
    public int Count => _intersections.Count;

    public IEnumerator<Intersection> GetEnumerator() => _intersections.GetEnumerator();
    IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();
}
