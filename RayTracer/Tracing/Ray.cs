﻿using RayTracer.Math;
using RayTracer.Math.Matrices;

namespace RayTracer.Tracing;

public readonly record struct Ray(Tuple Origin, Tuple Direction)
{
    public Tuple Position(double t) => Origin + Direction * t;

    public Ray Transform(Matrix44 transfrom) => new(
        transfrom * Origin,
        transfrom * Direction
    );
}
