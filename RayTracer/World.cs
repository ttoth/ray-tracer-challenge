﻿using RayTracer.Lights;
using RayTracer.Math;
using RayTracer.Shapes;
using RayTracer.Tracing;
using System.Collections.Generic;
using System.Linq;

namespace RayTracer;

public class World
{
    public const int MaxDepth = 5;

    public IList<PointLight> Lights { get; } = new List<PointLight>(1);
    public IList<IShape> Objects { get; } = new List<IShape>();

    public Intersections Intersect(Ray ray) => new(Objects
        .SelectMany(obj => obj.Intersect(ray))
        .OrderBy(i => i.T)
        .ToArray());

    public Tuple ShadeHit(ComputationData comps, int remaining)
    {
        var surface = Lights.Aggregate(
            Tuple.Color(0, 0, 0),
            (c, l) => c + comps.Object.Lighting(l, comps.OverPoint, comps.EyeV, comps.NormalV, IsShadowed(comps.OverPoint, l)));

        var reflected = ReflectedColor(comps, remaining);
        var refracted = RefractedColor(comps, remaining);
        var material = comps.Object.Material;

        if (material.Reflective > 0 && material.Transparency > 0)
        {
            var reflectance = comps.Schlick();
            return surface + reflected * reflectance +
                             refracted * (1 - reflectance);
        }

        return surface + reflected + refracted;
    }

    public Tuple ColorAt(Ray ray, int remaining = MaxDepth)
    {
        var intersections = Intersect(ray);
        var hit = intersections.Hit();
        if (hit is null)
        {
            return Tuple.Color(0, 0, 0);
        }

        var comps = hit.PrepareComputations(ray, intersections);
        return ShadeHit(comps, remaining);
    }

    public bool IsShadowed(Tuple point, PointLight light)
    {
        var v = light.Position - point;
        var distance = v.Magnitude();
        var direction = v.Normalize();

        var ray = new Ray(point, direction);
        var intersections = Intersect(ray);

        var hit = intersections.Hit();
        return hit != null && hit.T < distance;
    }

    public Tuple ReflectedColor(ComputationData comps, int remaining)
    {
        if (remaining == 0 || comps.Object.Material.Reflective == 0)
            return Tuple.Color(0, 0, 0);

        var reflectRay = new Ray(comps.OverPoint, comps.ReflectV);
        var color = ColorAt(reflectRay, remaining - 1);
        return color * comps.Object.Material.Reflective;
    }

    public Tuple RefractedColor(ComputationData comps, int remaining)
    {
        if (remaining == 0 || comps.Object.Material.Transparency == 0)
            return Tuple.Color(0, 0, 0);

        // Find the ratio of first index of refraction to the second.​
        // (Yup, this is inverted from the definition of Snell's Law.)​
        var nRatio = comps.N1 / comps.N2;

        //cos(theta_i) is the same as the dot product of the two vectors​
        var cosi = comps.EyeV.Dot(comps.NormalV);

        // Find sin(theta_t)^2 via trigonometric identity​
        var sin2t = nRatio * nRatio * (1 - cosi * cosi);

        if (sin2t > 1)
            return Tuple.Color(0, 0, 0);

        // Find cos(theta_t) via trigonometric identity​
        var cost = System.Math.Sqrt(1 - sin2t);

        // Compute the direction of the refracted ray
        var direction = comps.NormalV * (nRatio * cosi - cost) - comps.EyeV * nRatio;

        // Create the refracted ray
        var refractRay = new Ray(comps.UnderPoint, direction);

        // Find the color of the refracted ray, making sure to multiply by the transparency value to account for any opacity
        return ColorAt(refractRay, remaining - 1) * comps.Object.Material.Transparency;
    }
}
