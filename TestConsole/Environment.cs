﻿using RayTracer.Math;

namespace TestConsole;

struct Environment
{
    public Environment(Tuple gravity, Tuple wind)
    {
        Gravity = gravity;
        Wind = wind;
    }

    public Tuple Gravity { get; }
    public Tuple Wind { get; }
}
