﻿using RayTracer;
using RayTracer.Lights;
using RayTracer.Materials;
using RayTracer.Math;
using RayTracer.Patterns;
using RayTracer.Shapes;
using RayTracer.Tracing;
using System.Diagnostics;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using static RayTracer.Math.Matrices.Transformation;
using static RayTracer.Math.Tuple;
using static System.Math;

[assembly: ExcludeFromCodeCoverage]
namespace TestConsole;

class Program
{
    static void Main(string[] args)
    {
        var stopwatch = Stopwatch.StartNew();

        DrawTrajectory();
        DrawClock();
        DrawCircle();
        DrawWorld();

        System.Console.WriteLine(stopwatch.Elapsed.TotalSeconds);
    }

    static void DrawTrajectory()
    {
        var start = Point(0, 1, 0);
        var velocity = Vector(1, 1.8, 0).Normalize() * 112.5;
        var proj = new Projectile(start, velocity);

        var gravity = Vector(0, -9.81, 0);
        var wind = Vector(-1, 0, 0);
        var env = new Environment(gravity, wind);

        var canvas = new Canvas(900, 550);

        while (proj.Position.Y > 0)
        {
            proj = Tick(env, proj);

            if (proj.Position.X > 0 && proj.Position.Y > 1 &&
                proj.Position.X < canvas.Width - 1 && proj.Position.Y < canvas.Height)
                canvas.WritePixel((int)Round(proj.Position.X), canvas.Height - (int)Round(proj.Position.Y), Color(1, 0, 0));
        }

        File.WriteAllText("trajectory.ppm", canvas.ToPPM());

        static Projectile Tick(Environment env, Projectile proj)
        {
            const double deltaT = 0.05;
            var position = proj.Position + proj.Velocity * deltaT;
            var velocity = proj.Velocity + env.Gravity * deltaT + env.Wind * deltaT;
            return new Projectile(position, velocity);
        }
    }

    static void DrawClock()
    {
        const int Size = 80;
        var lenght = Size * 3d / 8;
        var canvas = new Canvas(Size, Size);
        var twelve = Point(0, 1, 0);

        var toCanvas = Translation(Size / 2d, Size / 2d, 0) * Scaling(-lenght, -lenght, 0);

        for (int i = 0; i < 12; ++i)
        {
            var transformed = toCanvas * RotationZ(PI / 6 * i) * twelve;
            canvas.WritePixel(ToInt(transformed.X), ToInt(transformed.Y), Color(i / 12d, 1 - i / 12d, i / 12d));
        }

        File.WriteAllText("clock.ppm", canvas.ToPPM());
        static int ToInt(double f) => (int)Round(f);
    }

    static void DrawCircle()
    {
        var rayOrigin = Point(0, 0, -5);
        var wallZ = 10;
        var wallSize = 7;
        var canvasPixels = 128;
        var pixelSize = wallSize / canvasPixels;
        var half = wallSize / 2;

        var canvas = new Canvas(canvasPixels, canvasPixels);
        var shape = new Sphere { Transform = RotationZ(PI / 4) * Scaling(1, 0.5, 1) };
        shape.Material.Color = Color(1, 0.2, 1);
        var light = new PointLight(Point(-10, 10, -10), Color(1, 1, 1));

        for (int y = 0; y < canvasPixels; ++y)
        {
            // compute the world y coordinate (top = +half, bottom = -half)​
            var worldY = half - pixelSize * y;

            for (int x = 0; x < canvasPixels; ++x)
            {
                // compute the world x coordinate (left = -half, right = half)​ ​
                var worldX = -half + pixelSize * x;

                // describe the point on the wall that the ray will target
                var position = Point(worldX, worldY, wallZ);

                var ray = new Ray(rayOrigin, (position - rayOrigin).Normalize());
                var xs = shape.Intersect(ray);

                var hit = xs.Hit();
                if (hit != null)
                {
                    var point = ray.Position(hit.T);
                    var normal = hit.Object.NormalAt(point);
                    var eye = -ray.Direction;
                    var color = hit.Object.Lighting(light, point, eye, normal, false);

                    canvas.WritePixel(x, y, color);
                }
            }
        }

        File.WriteAllText("circle.ppm", canvas.ToPPM());
    }

    static void DrawWorld()
    {
        var floor = new Plane
        {
            Material = new Material
            {
                Color = Color(1, 0.9, 0.9),
                Specular = 0
            }
        };

        var leftWall = new Plane
        {
            Transform = Translation(0, 0, 5) * RotationY(-PI / 4) * RotationX(-PI / 2),
            Material = floor.Material
        };

        var rightWall = new Plane
        {
            Transform = Translation(0, 0, 5) * RotationY(PI / 4) * RotationX(-PI / 2),
            Material = floor.Material
        };

        var middle = new Cube
        {
            Transform = Translation(-0.5, 0.8, 0.5) * RotationY(PI / 3) * Scaling(0.6, 0.8, 0.6),
            Material = new Material
            {
                Pattern = new RadialGradientPattern(Color(1, 0.9, 0.9), Color(0.1, 0.3, 0.3)),
                Diffuse = 0.7,
                Specular = 0.3
            }
        };

        var right = new Cone
        {
            Transform = Translation(1.5, 0.5, -0.5) * Scaling(0.5, 0.5, 0.5),
            Material = new Material
            {
                Color = Color(0, 0.3, 0.4),
                Ambient = 0.01,
                Diffuse = 0.5,
                Specular = 0.9,
                Shininess = 300,
                Reflective = 0.9,
                Transparency = 0.9,
                RefractiveIndex = 1.5
            },
            Minimum = -1,
            Maximum = 1,
            Closed = true
        };

        var left = new Sphere
        {
            Transform = Translation(-1.5, 0.6, -0.75) * Scaling(0.4, 0.6, 0.4),
            Material = new Material
            {
                Color = Color(1, 0.8, 1),
                Diffuse = 0.7,
                Specular = 0.3,
                Reflective = 0.7
            }
        };

        var world = new World();
        world.Lights.Add(new PointLight(Point(-10, 10, -10), Color(1, 1, 1)));
        world.Objects.Add(floor);
        world.Objects.Add(leftWall);
        world.Objects.Add(rightWall);
        world.Objects.Add(middle);
        world.Objects.Add(left);
        world.Objects.Add(right);

        var camera = new Camera(2000, 1000, PI / 3)
        {
            Transform = ViewTransform(Point(0, 1.5, -5), Point(0, 1, 0), Vector(0, 1, 0))
        };

        var canvas = camera.Render(world);
        File.WriteAllText("world.ppm", canvas.ToPPM());
    }
}
