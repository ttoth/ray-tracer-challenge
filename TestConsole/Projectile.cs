﻿using RayTracer.Math;

namespace TestConsole;

struct Projectile
{
    public Projectile(Tuple position, Tuple velocity)
    {
        Position = position;
        Velocity = velocity;
    }

    public Tuple Position { get; }
    public Tuple Velocity { get; }
}
